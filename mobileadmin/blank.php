<!DOCTYPE html>
<html>
<?php
   include 'static/vars.php';
   include_once("language/lang.php");
   //TODO
   $sm="2;4"; //Selected menu
?>
	<head>
	<title><?php __('blank_page'); ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="description" content="">
	<meta name="keywords" content="admin, bootstrap,admin template, bootstrap admin, simple, awesome">
	<meta name="author" content="">

	<!-- Bootstrap -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/third/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/style-responsive.css" rel="stylesheet">
	<link href="assets/css/animate.css" rel="stylesheet">
	<link href="assets/third/morris/morris.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/third/nifty-modal/css/component.css">
	<link rel="stylesheet" href="assets/third/sortable/sortable-theme-bootstrap.css"> 
	<link rel="stylesheet" href="assets/third/icheck/skins/minimal/grey.css"> 
	<link rel="stylesheet" href="assets/third/select/bootstrap-select.min.css"> 
	<link rel="stylesheet" href="assets/third/summernote/summernote.css" />
	<link rel="stylesheet" href="assets/third/magnific-popup/magnific-popup.css"> 
	<link rel="stylesheet" href="assets/third/pace/pace-theme-minimal.css">
	<link rel="stylesheet" href="assets/third/datepicker/css/datepicker.css"/>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="assets/img/favicon.ico">
	</head>
	<body class="tooltips">
	
	
	<!-- Begin page -->
	<div class="container">
		<!-- Modal Dialog -->
		<!-- Start sidebar menu -->
		<!-- Start right content -->
		<?php
		    include_once("static/modal.php");
   			include_once("static/navigation.php");
   			include_once("static/topbar.php");
		?>
		<!-- End of sidebar menu  and top bar-->
			
			
			
			<!-- ============================================================== -->
			<!-- Start Content here -->
			<!-- ============================================================== -->
            <div class="body content rows scroll-y">
				
				<!-- Page header -->
				<div class="page-heading animated fadeInDownBig">
					<h1>Blank <small>your master page</small></h1>
				</div>
				<!-- End page header -->
				
				
				<!-- Your awesome content goes here -->
				
				
				<p>Your awesome content goes here...</p>
				
				
				<!-- End of your awesome content -->
			
			
            </div>
			<!-- ============================================================== -->
			<!-- End content here -->
			<!-- ============================================================== -->	
        </div>
		<!-- End right content -->
		
		
		
		
		
		<!-- the overlay modal element -->
		<div class="md-overlay"></div>
		<!-- End of eoverlay modal -->
		
		
		
	</div>
	<!-- End of page -->

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="assets/js/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/third/knob/jquery.knob.js"></script>
		<script src="assets/third/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="assets/third/morris/morris.js"></script>
		<script src="assets/third/nifty-modal/js/classie.js"></script>
		<script src="assets/third/nifty-modal/js/modalEffects.js"></script>
		<script src="assets/third/sortable/sortable.min.js"></script>
		<script src="assets/third/select/bootstrap-select.min.js"></script>
		<script src="assets/third/summernote/summernote.js"></script>
		<script src="assets/third/magnific-popup/jquery.magnific-popup.min.js"></script> 
		<script src="assets/third/pace/pace.min.js"></script>
		<script src="assets/third/input/bootstrap.file-input.js"></script>
		<script src="assets/third/datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/third/icheck/icheck.min.js"></script>
		<script src="assets/third/wizard/jquery.easyWizard.js"></script>
		<script src="assets/third/wizard/scripts.js"></script>
		<script src="assets/js/lanceng.js"></script>

		<!-- Cloud connect scripts -->
		<script type="text/javascript" src="assets/scripts/cocoafish-1.2.js"></script> 
    	<script type="text/javascript" src="assets/scripts/utils.js"></script>    
	</body>
</html>