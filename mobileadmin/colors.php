<!DOCTYPE html>
<html>
<?php
   include 'static/vars.php';
   include_once("language/lang.php");
   //TODO
   $sm="8;3"; //Selected menu
?>
	<head>
	<title><?php __('colors'); ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="description" content="">
	<meta name="keywords" content="admin, bootstrap,admin template, bootstrap admin, simple, awesome">
	<meta name="author" content="">

	<!-- Bootstrap -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/third/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/style-responsive.css" rel="stylesheet">
	<link href="assets/css/animate.css" rel="stylesheet">
	<link href="assets/third/morris/morris.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/third/nifty-modal/css/component.css">
	<link rel="stylesheet" href="assets/third/sortable/sortable-theme-bootstrap.css"> 
	<link rel="stylesheet" href="assets/third/icheck/skins/minimal/grey.css"> 
	<link rel="stylesheet" href="assets/third/select/bootstrap-select.min.css"> 
	<link rel="stylesheet" href="assets/third/summernote/summernote.css" />
	<link rel="stylesheet" href="assets/third/magnific-popup/magnific-popup.css"> 
	<link rel="stylesheet" href="assets/third/pace/pace-theme-minimal.css">
	<link rel="stylesheet" href="assets/third/datepicker/css/datepicker.css"/>
	<link rel="stylesheet" type="text/css" href="assets/third/bootstrap-colorpicker/css/colorpicker.css" />

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="assets/img/favicon.ico">
	</head>
	<body class="tooltips">
	
	
	<!-- Begin page -->
	<div class="container">
		<!-- Modal Dialog -->
		<!-- Start sidebar menu -->
		<!-- Start right content -->
		<?php
		    include_once("static/modal.php");
   			include_once("static/navigation.php");
   			include_once("static/topbar.php");
		?>
		<!-- End of sidebar menu  and top bar-->
			
			
			
			<!-- ============================================================== -->
			<!-- Start Content here -->
			<!-- ============================================================== -->
            <div class="body content rows scroll-y">
				
				<!-- Page header -->
				<div class="page-heading animated fadeInDownBig">
					<h1><?php __('color_page_title'); ?></h1>
				</div>
				<!-- End page header -->


				
				
				<!-- Your awesome content goes here -->
				
				
				<div class="box-info animated fadeInDown">
				<h2><?php __('color_form_title'); ?></h2>
					<form class="form" role="form">
						
					  <?php 
					  	colorFieldGenerator(___('windowBackgroundColor'),"windowBackgroundColor","#400000");
					  	colorFieldGenerator(___('barColor'),"barColor","#400000");
					  	colorFieldGenerator(___('separatoColor'),"separatoColor","#e9e8d9");
					  	colorFieldGenerator(___('rowColor'),"rowColor","#FFFFFF");
					  	colorFieldGenerator(___('rowOddColor'),"rowOddColor","#e7b0b0");
					  	colorFieldGenerator(___('textColor'),"textColor","#3a3a3a");
					  	colorFieldGenerator(___('textSecondColor'),"textSecondColor","#CCCCCC");
					  	colorFieldGenerator(___('reloadBackgroundColor'),"reloadBackgroundColor","#f0f0f0");
					  	colorFieldGenerator(___('reloadTextColor'),"reloadTextColor","#444444");
					  	colorFieldGenerator(___('buttonColor'),"buttonColor","#010101");
					  	colorFieldGenerator(___('buttonTopColor'),"buttonTopColor","#010101");
					  	colorFieldGenerator(___('buttonBottomColor'),"buttonBottomColor","#333333");
					  	colorFieldGenerator(___('tabTintColor'),"tabTintColor","#FFFFFF");					  	
					   ?>
					

					    <div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
									  <a id="save_button"  class="btn btn-default saveMenu"><?php __('save');?></a><img  class="loader_save_update" src="assets/img/ajax-loader.gif" />
									</div>
								  </div>

					</form>
				</div>
				
				
				<!-- End of your awesome content -->
			
			
            </div>
			<!-- ============================================================== -->
			<!-- End content here -->
			<!-- ============================================================== -->	
        </div>
		<!-- End right content -->
		
		
		
		
		
		<!-- the overlay modal element -->
		<div class="md-overlay"></div>
		<!-- End of eoverlay modal -->
		
		
		
	</div>
	<!-- End of page -->

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="assets/js/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/third/knob/jquery.knob.js"></script>
		<script src="assets/third/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="assets/third/morris/morris.js"></script>
		<script src="assets/third/nifty-modal/js/classie.js"></script>
		<script src="assets/third/nifty-modal/js/modalEffects.js"></script>
		<script src="assets/third/sortable/sortable.min.js"></script>
		<script src="assets/third/select/bootstrap-select.min.js"></script>
		<script src="assets/third/summernote/summernote.js"></script>
		<script src="assets/third/magnific-popup/jquery.magnific-popup.min.js"></script> 
		<script src="assets/third/pace/pace.min.js"></script>
		<script src="assets/third/input/bootstrap.file-input.js"></script>
		<script src="assets/third/datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/third/icheck/icheck.min.js"></script>
		<script src="assets/third/wizard/jquery.easyWizard.js"></script>
		<script type="text/javascript" src="assets/third/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> 
		<script src="assets/third/wizard/scripts.js"></script>
		<script src="assets/js/lanceng.js"></script>

		<!-- Cloud connect scripts -->
		<script type="text/javascript" src="assets/scripts/cocoafish-1.2.js"></script> 
    	<script type="text/javascript" src="assets/scripts/utils.js"></script>   
    	<script type="text/javascript" src="assets/scripts/cloudconnect/objects.js"></script>

    	<script>

  var settings=null;
  function fetchAndUpdateSettings(){
  	$('.loader_save_update').show();
   retreiveObjects({
    objectClass:"settings",
    query:{response_json_depth:1,page:1,per_page:50},
    listener:function(data){
      setUpSettings({
        items:data,
      })
    }
  })
 };



 function saveSettings(){
 	$('.loader_save_update').show();
  if(settings!=null){
    //We can save

    var fields={
      windowBackgroundColor:$('#windowBackgroundColor').val(),
      barColor:$('#barColor').val(),
      separatoColor:$('#separatoColor').val(),
      rowColor:$('#rowColor').val(),
      rowOddColor:$('#rowOddColor').val(),
      textColor:$('#textColor').val(),
      textSecondColor:$('#textSecondColor').val(),
      reloadBackgroundColor:$('#reloadBackgroundColor').val(),
      reloadTextColor:$('#reloadTextColor').val(),
      buttonColor:$('#buttonColor').val(),
      buttonTopColor:$('#buttonTopColor').val(),
      buttonBottomColor:$('#buttonBottomColor').val(),
      tabTintColor:$('#tabTintColor').val(),
    }
    

    if(settings.id){
      //We have settings before
      console.log('Modify existing settings');
      settings.colors=fields;
      updateClassObject({
        objectClass:"settings",
        fields:settings,
        id:settings.id,
        photo:null,
        listener:function(e){
        	$('.loader_save_update').hide();
        }
      });
    }else{
      console.log('Create new settings');
      topublish={
        colors:fields
      }
      console.log(topublish);

      addClassObject({
        objectClass:"settings",
        fields:topublish,
        photo:null,
        listener:function(e){
          window.location = 'colors.php';
        }
      });

    }
  }else{
      alert("Please wait until the settings are fetch!")
    }
  }

function setUpSettings(data){
 $('.loader_save_update').hide();
 console.log(data.items.length);
 if(data.items.length>0){
  settings=data.items[0];

  if(settings.colors){
    colors=settings.colors;

    $('#windowBackgroundColor').val(colors.windowBackgroundColor);
    $('#barColor').val(colors.barColor);
    $('#separatoColor').val(colors.separatoColor);
    $('#rowColor').val(colors.rowColor);
    $('#rowOddColor').val(colors.rowOddColor);
    $('#textColor').val(colors.textColor);
    $('#textSecondColor').val(colors.textSecondColor);
    $('#reloadBackgroundColor').val(colors.reloadBackgroundColor);
    $('#reloadTextColor').val(colors.reloadTextColor);
    $('#buttonColor').val(colors.buttonColor);
    $('#buttonTopColor').val(colors.buttonTopColor);
    $('#buttonBottomColor').val(colors.buttonBottomColor);
    $('#tabTintColor').val(colors.tabTintColor);

    $('#windowBackgroundColorDiv').colorpicker('setValue',colors.windowBackgroundColor);
    $('#barColorDiv').colorpicker('setValue',colors.barColor);
    $('#separatoColorDiv').colorpicker('setValue',colors.separatoColor);
    $('#rowColorDiv').colorpicker('setValue',colors.rowColor);
    $('#rowOddColorDiv').colorpicker('setValue',colors.rowOddColor);
    $('#textColorDiv').colorpicker('setValue',colors.textColor);
    $('#textSecondColorDiv').colorpicker('setValue',colors.textSecondColor);
    $('#reloadBackgroundColorDiv').colorpicker('setValue',colors.reloadBackgroundColor);
    $('#reloadTextColorDiv').colorpicker('setValue',colors.reloadTextColor);
    $('#buttonColorDiv').colorpicker('setValue',colors.buttonColor);
    $('#buttonTopColorDiv').colorpicker('setValue',colors.buttonTopColor);
    $('#buttonBottomColorDiv').colorpicker('setValue',colors.buttonBottomColor);
    $('#tabTintColorDiv').colorpicker('setValue',colors.tabTintColor);
  }
  
}else{
  settings={};
}
};

    		jQuery(document).ready(function() { 
    			    $('.color').colorpicker();
    			    fetchAndUpdateSettings();
    			    $('a.saveMenu').click(saveSettings);
    		});
		</script>
	</body>
</html>