/**
 * Retreives list of categories and return the categories only 
 * @param {Object} p
 * @config {String} objectClass
 * @config {Object} query
 * @config {Function} listener, Listener for the result
 */
function retreiveObjects(p)
{
	sdk.sendRequest('objects/'+p.objectClass+'/query.json', 'GET', p.query, function(responseData) {
		p.listener(responseData.response[p.objectClass]);
	});
}

/**
 * Retreives list of categories and return the categories only 
 * @param {Object} p
 * @config {String} objectClass
 * @config {Object} query
 * @config {Function} listener, Listener for the result
 */
function retreiveObjectsFull(p)
{
	sdk.sendRequest('objects/'+p.objectClass+'/query.json', 'GET', p.query, function(responseData) {
		p.listener(responseData);
	});
}

/**
 * @param {Object} p
 * @config {String} objectClass, name of the object
 * @config {Object} fields
 * @config {Blob} photo
 * @config {Function}, listener for the correct result
 */
function addClassObject(p)
{
	var dataToSend={
		fields:JSON.stringify(p.fields),
		photo:p.photo,
	}
	sdk.sendRequest('objects/'+p.objectClass+'/create.json', 'POST',dataToSend, function(responseData) {
		p.listener(responseData);
	});
}

/**
 * @param {Object} p
 * @config {String} objectClass, name of the object
 * @config {Object} fields
 * @config {String} id
 * @config {Blob} photo
 * @config {Function}, listener for the correct result
 */
function updateClassObject(p)
{
	var dataToSend={
		fields:JSON.stringify(p.fields),
		photo:p.photo,
		id:p.id,
	}
	sdk.sendRequest('objects/'+p.objectClass+'/update.json', 'PUT',dataToSend, function(responseData) {
		p.listener(responseData);
	});
}

/**
 * @param {Object} p
 * @config {Number} id, the id of the category
 * @config {Function}, Listener for the correct result
 * @config {String} objectClass
 */
function deleteClassObject(p)
{
	sdk.sendRequest('objects/'+p.objectClass+'/delete.json', 'DELETE', {id:p.id}, function(responseData) {
		p.listener(responseData);
	});
}

/**
 * Sets the categories to the drop down of categories 
 * @param {Object} p
 */
function setCategoriesToElement(p)
{
	retreiveCategories({
		listener:function(categories)
		{
			var options = '';
      		for (var i = 0; i < categories.length; i++) 
      		{
      			if(categories[i].lang==p.lang)
      			{
      				options += '<option value="' + categories[i].id + '">' + categories[i].name + '</option>';
      			}
        		
     		 }
      		p.element.html(options);
      		p.listener(categories);
      		return categories;

		}
	})
}
