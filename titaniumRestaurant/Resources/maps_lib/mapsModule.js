/*
* @author DANIEL DIMOV
* @email dimovdaniel@yahoo.com
* @content maps Module
* @version 2.0
*/

//Include Required modules
var VARS =require('common/globals');
var GUI =require('maps_lib/mapGUI');    

/**
 * @description Creates window with map
 * @param {Object} p, Parameters
 * @config {Window} win, the window to open the gallery in
 * @config {String} title, the window title
 * @config {Int} type, type of the distance [0-In kilometers , 1 - in Milles ]
 * @config {Int} mapType, type of the map [0-In kilometers , 1 - in Milles ]
 * @config {String} tab, tab to open sub windows
 * @config {String} locations, Location Object
 */
exports.createMap = function(p) 
{

//################ DATA ###################
    var pointAdded=false;
	var MapModule = require('ti.map');

	//Array for the locations
	var locations = [];

	for( i = 0; i < p.locations.length; i++) 
	{	
		var l = MapModule.createAnnotation({
			latitude : p.locations[i].latlon.split(',')[0],
			longitude : p.locations[i].latlon.split(',')[1],
			title : p.locations[i].title,
			subtitle : p.locations[i].subTitle,
			pincolor : MapModule.ANNOTATION_RED,
			animate : true,
			rightButton : Titanium.UI.iPhone.SystemButton.INFO_LIGHT,
			rightView:"/images/place.png",
			myid : i // CUSTOM ATTRIBUTE THAT IS PASSED INTO EVENT OBJECTS
		});
		locations.push(l);
	}

	//############### VIEWS #####################
	var mapview = MapModule.createView({
		//mapType : p.mapType,
		region : {
			latitude : p.locations[0].latlon.split(',')[0],
			longitude : p.locations[0].latlon.split(',')[1],
			latitudeDelta : 0.5,
			longitudeDelta : 0.5
		},
		animate : true,
		regionFit : true,
		userLocation : true,
	});
	
	
	
	
	
	
	

	//#################### FUNCTIONS ###################


	//#################### EVENT LISTENRS ##############
   
    mapview.addEventListener('click', function(e) {
        if (e.clicksource == 'infoWindow' || e.clicksource == 'subtitle' ||e.clicksource == 'title' || e.clicksource == 'rightButton' || e.clicksource == 'leftPane' || e.clicksource == 'rightView') {
            if (VARS._platform == VARS._android) {
                var intent = Ti.Android.createIntent({
                    action : Ti.Android.ACTION_VIEW,
                    data : 'geo:'+e.annotation.latitude+','+e.annotation.longitude
                });
                Ti.Android.currentActivity.startActivity(intent);
                //Ti.Platform.openURL("http://maps.google.com/maps?q=" + e.annotation.latitude + "," + e.annotation.longitude);
            } else {
                Ti.Platform.openURL("http://maps.google.com/maps?q=" + e.latitude + "," + e.longitude);
            }

        }
    })

	
	mapview.addEventListener('postlayout',function(e){
	    mapview.addAnnotations(locations);
	    /*if(VARS._iOS){
	        mapview.addAnnotations(locations);
	    }else if(!pointAdded){
	        pointAdded=true;
	         mapview.addAnnotations(locations);
	    }*/

	    
	})

	p.win.add(mapview);
	


}


