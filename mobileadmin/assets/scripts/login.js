var Login = function () {
        return {
            init: function () {
                $(".login-form").submit(function (e) {
                    e.preventDefault()
                });
                $(".login-form").validate({
                    errorElement: "label",
                    errorClass: "help-inline",
                    focusInvalid: false,
                    rules: {
                        username: {
                            required: true
                        },
                        password: {
                            required: true
                        }
                    },
                    messages: {
                        username: {
                            required: "Email is required."
                        },
                        password: {
                            required: "Password is required."
                        }
                    },
                    invalidHandler: function (e, t) {
                        $(".alert-error", $(".login-form")).show()
                    },
                    highlight: function (e) {
                        $(e).closest(".form-control").addClass("error")
                    },
                    success: function (e) {
                        e.closest(".form-control").removeClass("error");
                        e.remove()
                    },
                    errorPlacement: function (e, t) {
                        e.addClass("help-small no-left-padding").insertAfter(t.closest(".input-icon"))
                    },
                    submitHandler: function (e) {
                        var t = $("form").find("#email").val();
                        var n = $("form").find("#password").val();
                        console.log("Login:" + t);
                        console.log("pass:" + n);
                        $.ajax({
                            dataType: "json",
                            url: "api/proxy.php",
                            data: {
                                userName: t,
                                action: "remotelogin",
                                purchaseCode: purchaseCode,
                                apiKey: apiKey
                            },
                            success: function (e) {
                                if (e.status) {
                                    appkey = e.api_key;
                                    var r = new Cocoafish(appkey);
                                    r.sendRequest("users/login.json", "POST", {
                                        login: t,
                                        password: n
                                    }, function (t) {
                                        if (t && t.meta && t.meta.code == 200) {
                                            if (t.response && t.response.users && t.response.users[0].admin == "true") {
                                                var n = t.response.users[0].first_name;
                                                var r = t.response.users[0].last_name;
                                                var i = "";
                                                if (t.response.users[0].photo && t.response.users[0].photo.urls && t.response.users[0].photo.urls.thumb_100) {
                                                    i = t.response.users[0].photo.urls.thumb_100
                                                }
                                                setCookie("_username", n, null);
                                                setCookie("_userid", t.response.users[0].id, null);
                                                setCookie("_appkey", e.api_key, null);
                                                var s = "index.php?adminName=" + n + " " + r;
                                                if (i != "") {
                                                    s += "&adminPhoto=" + i
                                                }
                                                console.log(s);
                                                window.location = s
                                            } else {
                                                remove_cookie("_session_id");
                                                alert("Plese login with valid admin user")
                                            }
                                        } else {
                                            if (t && t.meta && t.meta.message) {
                                                alert(t.meta.message)
                                            } else {
                                                alert("Unknown error occured! Please check all parameters.")
                                            }
                                        }
                                    })
                                } else {
                                    alert(e.log)
                                }
                                console.log(e)
                            }
                        })
                    }
                })
            }
        }
    }()