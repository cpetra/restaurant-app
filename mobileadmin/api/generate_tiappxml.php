<?php


$xml = '<?xml version="1.0" encoding="UTF-8"?>
<ti:app xmlns:ti="http://ti.appcelerator.org">
    <property name="acs-oauth-secret-production" type="string">'.$_POST["acs-oauth-secret-production"].'</property>
    <property name="acs-oauth-key-production" type="string">'.$_POST["acs-oauth-key-production"].'</property>
    <property name="acs-api-key-production" type="string">'.$_POST["acs-api-key-production"].'</property>
    <property name="acs-oauth-secret-development" type="string">'.$_POST["acs-oauth-secret-production"].'</property>
    <property name="acs-oauth-key-development" type="string">'.$_POST["acs-oauth-key-production"].'</property>
    <property name="acs-api-key-development" type="string">'.$_POST["acs-api-key-production"].'</property>
    <property name="ti.deploytype">production</property>
    <id>'.$_POST["app_id"].'</id>
    <name>'.$_POST["app_name"].'</name>
    <version>1.0</version>
    <publisher>NextWebArt</publisher>
    <url>http://nextwebart.com</url>
    <description>not specified</description>
    <copyright>2014 by NextWebArt</copyright>
    <icon>appicon.png</icon>
    <persistent-wifi>false</persistent-wifi>
    <prerendered-icon>false</prerendered-icon>
    <statusbar-style>default</statusbar-style>
    <statusbar-hidden>false</statusbar-hidden>
    <fullscreen>false</fullscreen>
    <navbar-hidden>false</navbar-hidden>
    <analytics>false</analytics>
    <guid>'.$_POST["guid"].'</guid>
    <property name="ti.ui.defaultunit" type="string">system</property>
    <property name="ti.android.bug2373.finishfalseroot" type="bool">true</property>
    <ios>
        <plist>
            <dict>
                <key>UIStatusBarStyle</key>
                <string>UIStatusBarStyleLightContent</string>
            </dict>
        </plist>
    </ios>
    <iphone>
        <orientations device="iphone">
            <orientation>Ti.UI.PORTRAIT</orientation>
        </orientations>
        <orientations device="ipad">
            <orientation>Ti.UI.PORTRAIT</orientation>
            <orientation>Ti.UI.UPSIDE_PORTRAIT</orientation>
            <orientation>Ti.UI.LANDSCAPE_LEFT</orientation>
            <orientation>Ti.UI.LANDSCAPE_RIGHT</orientation>
        </orientations>
    </iphone>
    <android xmlns:android="http://schemas.android.com/apk/res/android">
        <manifest android:installLocation="preferExternal">
            <!-- Allows the API to download data from Google Map servers -->
            <uses-permission android:name="android.permission.INTERNET"/>
            <!-- Allows the API to cache data -->
            <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
            <!-- Use GPS for device location -->
            <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
            <!-- Use Wi-Fi or mobile connection for device location -->
            <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
            <!-- Allows the API to access Google web-based services -->
            <uses-permission android:name="com.google.android.providers.gsf.permission.READ_GSERVICES"/>
            <!-- Specify OpenGL ES 2.0 as a requirement -->
            <uses-feature android:glEsVersion="0x00020000" android:required="true"/>
             <!-- Replace <com.domain.appid> with your application ID -->
            <uses-permission android:name="'.$_POST["app_id"].'.permission.MAPS_RECEIVE"/>
            <permission
                android:name="'.$_POST["app_id"].'.permission.MAPS_RECEIVE" android:protectionLevel="signature"/>
            <application>
                <!-- Replace "PASTE YOUR GOOGLE MAPS API KEY HERE" with the Google API key you obtained -->
                <meta-data
                    android:name="com.google.android.maps.v2.API_KEY" android:value="PASTE YOUR GOOGLE MAPS API KEY HERE"/>
            </application>
        </manifest>
    </android>
    <mobileweb>
        <precache/>
        <splash>
            <enabled>true</enabled>
            <inline-css-images>true</inline-css-images>
        </splash>
        <theme>default</theme>
    </mobileweb>
    <modules>
        <module platform="android">ti.admob</module>
        <module platform="iphone">ti.admob</module>
        <module platform="android">facebook</module>
        <module platform="iphone">facebook</module>
        <module platform="commonjs">ti.cloud</module>
        <module platform="iphone">ti.map</module>
        <module platform="android">ti.map</module>
        <module platform="android">ti.paypal</module>
        <module platform="iphone">ti.paypal</module>
        <module platform="android">ti.cloudpush</module>
    </modules>
    <deployment-targets>
        <target device="android">true</target>
        <target device="blackberry">false</target>
        <target device="ipad">true</target>
        <target device="iphone">true</target>
        <target device="mobileweb">false</target>
        <target device="tizen">false</target>
    </deployment-targets>
    <sdk-version>3.2.2.GA</sdk-version>
</ti:app>';


Header('Content-type: text/xml');
header('Content-Disposition: attachment; filename="tiapp.xml"');
print($xml);
?>