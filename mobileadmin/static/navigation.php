<?php 

function checkActive($me,$current,$top=true,$select=false,$open=false){
	$currentMenu=explode(';', $current);
	if($top){
		if($me==$currentMenu[0]){
			if($select){
				return " visible ";
			}
			if($open){
				return " open ";
			}
			return " active ";
		}
	}else{
		if($me==$current){
			if($select){
				return " select ";
			}
			if($open){
				return " open ";
			}
			return " active ";
		}
	}
}

function checkAvailable($index,$extraMenus){
	$menus = explode(";", $extraMenus);
	
	for($i=0;$i<count($menus);$i++){
		if($menus[$i]==$index){
			return TRUE;
		}
	}

	return FALSE;

}

echo '<!-- Start sidebar menu -->
		<div class="left side-menu">
            <div class="header sidebar rows">
				<div class="logo animated bounceIn">
					<h1><a href=""><img src="assets/img/logo.png" alt="Logo"> '.___('page_title').'</a></h1>
				</div>
            </div>
            <div class="body rows scroll-y animated fadeInLeftBig">
                <div class="sidebar-inner slimscroller">
					<div class="media">
					  <a class="pull-left" href="#">
						<img class="media-object img-circle" src="'.$adminPhoto.'" alt="Avatar">
					  </a>
					  <div class="media-body">
					  '. ___('admin_salut').',
						<h4 class="media-heading"><strong>'.$adminName.'</strong></h4>
						<a class="md-trigger" data-modal="logout-modal-alt">'.___('logout').'</a>
					  </div>
					</div>
				
					
				<div id="sidebar-menu">
					<ul>
						<li class="'.checkActive("1",$sm).'"><a href="index.php"><i class="fa fa-home"></i> '.___('dashboard').'</a></li>
						<li class="'.checkActive("2",$sm).'"><a href="menu.php"><i class="fa fa-cutlery"></i><i class="fa fa-angle-double-down i-right"></i> '.___('menu').'</a>
							<ul class="'.checkActive("2",$sm,true,true,false).'">
								<li class="'.checkActive("2;1",$sm,false,false,false).'"><a href="menu.php"><i class="fa fa-angle-right"></i> '.___('menu_items').'</a></li>
								<li class="'.checkActive("2;2",$sm,false,false,false).'"><a href="menucategory.php"><i class="fa fa-angle-right"></i> '.___('menu_cat').'</a></li>
								<li class="'.checkActive("2;3",$sm,false,false,false).'"><a href="menupayment.php"><i class="fa fa-angle-right"></i> '.___('menu_pay').'</a></li>
								<li class="'.checkActive("2;4",$sm,false,false,false).'"><a href="menuorders.php"><i class="fa fa-angle-right"></i> '.___('menu_orders').'</a></li>
							</ul>
						</li>
						<li class="'.checkActive("3",$sm).'"><a href="gallery.php"><i class="fa fa-picture-o "></i> '.___('gallery').'</a></li>
						<li class="'.checkActive("4",$sm).'"><a href="news.php"><i class="fa fa-pencil-square-o "></i> '.___('news').'</a></li>
						<li class="'.checkActive("5",$sm).'"><a href="about.php"><i class="fa fa-info"></i> '.___('about').'</a></li>
						<li class="'.checkActive("6",$sm).'"><a href="contact.php"><i class="fa fa-mobile"></i> '.___('contact').'</a></li>
						<li class="'.checkActive("7",$sm).'"><a href="location.php"><i class="fa fa-map-marker"></i> '.___('location').'</a></li>
						<li class="'.checkActive("9",$sm).'"><a href="push.php"><i class="fa fa-bullhorn"></i> '.___('push').'</a></li>
						<li class="'.checkActive("8",$sm).'"><a href="settings.php"><i class="fa fa-cog"></i><i class="fa fa-angle-double-down i-right"></i> '.___('settings').'</a>
							<ul class="'.checkActive("8",$sm,true,true,false).'">
								<li class="'.checkActive("8;1",$sm,false,false,false).'"><a href="settings.php"><i class="fa fa-angle-right"></i> '.___('gen_settings').'</a></li>
								<li class="'.checkActive("8;2",$sm,false,false,false).'"><a href="reposition.php"><i class="fa fa-angle-right"></i> '.___('reposition_navigation').'</a></li>
								<li class="'.checkActive("8;3",$sm,false,false,false).'"><a href="colors.php"><i class="fa fa-angle-right"></i> '.___('colors').'</a></li>
								<li class="'.checkActive("8;4",$sm,false,false,false).'"><a href="tiapp.php"><i class="fa fa-angle-right"></i> '.___('tiapp_download').'</a></li>
								
							</ul>
						</li>						
					</ul>
					<div class="clear"></div>
				</div>
				</div>
            </div>
            <div class="footer rows animated fadeInUpBig">
				
            </div>
        </div>
		<!-- End of sidebar menu -->';
?>