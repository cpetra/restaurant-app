/*
* @author DANIEL DIMOV
* @email dimovdaniel@yahoo.com
* @content Contains GUI creators and default styles
*/

var VARS =require('common/globals');
var GUI =require('common/gui');

//######################### COLORS ##################
var tableBackgroundColor="black";

//######################## SIZES ####################
var rowHeight="65dp";
var imageHeight="55dp";
var imageWidth="55dp";

//############# NORMAL NEWS0 ROW CREATOR ##################
var HtmlCodesDecode=function(str)
{
	str=str.replace(/&#8220;/g,'"');
	str=str.replace(/&#8216;/g,'\'');
	str=str.replace(/&#8221;/g,'"');
	str=str.replace(/&#8217;/g,'\'');
	str=str.replace(/&#8211;/g,'-');
	return str;
	
}
exports.creteSimpleRow=function(p)
{
	//The Row
	var bgColor=VARS.rowColor;
	if(p.index%2==1){bgColor=VARS.rowOddColor}
	var row = Ti.UI.createTableViewRow({
		height:rowHeight,
		hasChild:VARS._platform!=VARS._android,
		backgroundColor:bgColor});
	row.className = 'datarow';
	row.clickName = 'row';
	row.index = p.index;
	row.data=p;
	
	//The icon
	var icon=Ti.UI.createImageView({
		image:VARS._imageNewsDefault,
		touchEnabled:false,
		height:imageHeight,
		width:imageWidth,
		left:"5dp",
		top:"5dp",
		borderRadius:5,
	})
	icon.image=p.avatar;
	row.add(icon);
	
	//The title
	var textW=(VARS._dpiWidth-100)+"dp";
	var theTitle = Ti.UI.createLabel({
		text:HtmlCodesDecode(p.title),
		left:"70dp",
		height:"20dp",
		top:"0dp",
		font:VARS.h2,
		width:textW,
		color:VARS.textColor,
		touchEnabled:false,
	})
	row.add(theTitle);
	var textTop="20dp";
	var textHeight="32dp"
	if(VARS._platform==VARS._android){textHeight="27dp"}
	if(p.title=="")
	{
		textTop="0dp";
		textHeight="52dp";
	}
	var theText = Ti.UI.createLabel({
		text:HtmlCodesDecode(p.text),
		value:p.text,
		left:"70dp",
		height:textHeight,
		top:textTop,
		font:VARS.normal,
		width:textW,
		color:VARS.textColor,
		touchEnabled:false,
	})
	row.add(theText);
	
	var theDate = Ti.UI.createLabel({
		text:p.created_at,
		left:"70dp",
		height:"15dp",
		bottom:"0dp",
		font:VARS.empahasys,
		width:textW,
		color:VARS.textSecondColor,
		touchEnabled:false,
	})
	row.add(theDate);
	
	return row;
}


exports.creteSimpleCategoryRow=function(p)
{
	//The Row
	var bgColor=VARS.rowColor;
	//if(p.index%2==1){bgColor=VARS.rowOddColor}
	var row = Ti.UI.createTableViewRow({
		height:"40dp",
		hasChild:VARS._platform!=VARS._android,
		backgroundColor:bgColor});
	row.className = 'datarow';
	row.clickName = 'row';
	row.index = p.index;
	row.data=p;
	
	
	//The left strip
	var strip=Ti.UI.createView({
		top:0,
		left:0,
		width:"5dp",
		height:"40dp",
		touchEnabled:false,
		backgroundColor:VARS.listColors[p.index%VARS.listColors.length],
	})
	row.add(strip);
	
	
	
	//The title
	var textW="220dp";
	if(VARS._platform==VARS._iPad)
	{
		textW="668";
	}
	else if(VARS._platform==VARS._android)
	{
		textW="230dp";
	}
	var theTitle = Ti.UI.createLabel({
		text:HtmlCodesDecode(p.title),
		left:"15dp",
		height:"40dp",
		top:"0dp",
		font:VARS.h2,
		width:textW,
		color:VARS.textColor,
		touchEnabled:false,
	})
	row.add(theTitle);
	
	return row;
}

/**
 * Create twitter like banner
 * @param {Object} p
 * @config {Object} user, The user to create the banner for
 */
var userBannerCreator=function(p)
{
    index=VARS._dpiWidth/520;
    viewHeight=(260*index)+"dp";
    
    var mainHolder=Ti.UI.createView({
        width:VARS._dpiWidth+"dp",
        height:viewHeight,
        top:"0dp"
    })
    var backgroundImage=Ti.UI.createImageView({
        image:p.user.profile_banner_url+"/web",
        preventDefaultImage:true,
        width:VARS._dpiWidth+"dp",
        height:viewHeight,
        top:"0dp"
        
    })  
    var blackCover=Ti.UI.createView({
        width:VARS._dpiWidth+"dp",
        height:viewHeight,
        backgroundColor:"#AA000000",
        opacity:0,
    })
    mainHolder.add(backgroundImage);
    mainHolder.add(blackCover);
    
    //Image,name and username
    var firstBannerView=Ti.UI.createView({
        width:VARS._dpiWidth+"dp",
        height:viewHeight,
    })
    
    var firstHalfFirst=Ti.UI.createView({
        height:"60%",
        top:"0dp",
    })
    
    var profileImage=VARS._iOS?p.user.profile_image_url.replace("_normal",""):p.user.profile_image_url.replace("_normal","_bigger");
    
        Ti.API.info(profileImage);
    var profileImage=Ti.UI.createImageView({
        image:profileImage,
        preventDefaultImage:true,
        height:"55dp",
        width:"55dp",
        borderRadius:"3dp",
        borderWidth:"3dp",
        borderColor:"white",
    })
    firstHalfFirst.add(profileImage);
    firstBannerView.add(firstHalfFirst);
    
    
    var secondHalfFirst=Ti.UI.createView({
        height:"40%",
        bottom:"0dp",
    })

    var name=Ti.UI.createLabel({
        font:VARS.h1,
        color:"white",
        text:p.user.name,
        top:"0dp"
    })
    secondHalfFirst.add(name)
    var screenName=Ti.UI.createLabel({
        font:{fontSize:"12dp"},
        color:"white",
        text:"@"+p.user.screen_name,
        top:"22dp"
    })
    secondHalfFirst.add(screenName)
    firstBannerView.add(secondHalfFirst);
    
    //Second banner view. description and link
    var secondBannerView=Ti.UI.createView({
        width:VARS._dpiWidth+"dp",
        height:viewHeight,
        backgroundColor:"#00000000",
    })
    var description=Ti.UI.createLabel({
        text:p.user.description,
        width:"80%",
        font:{fontSize:"12dp"},
        color:"white",
        textAlign:"center",
    })
    secondBannerView.add(description);
    
    //Third banner view. link and followers
    var thirdBannerView=Ti.UI.createView({
        width:VARS._dpiWidth+"dp",
        height:viewHeight,
        backgroundColor:"#00000000",
    })
    var url=Ti.UI.createLabel({
        text:p.user.url.replace("http://",""),
        width:"80%",
        font:VARS.h3,
        color:"white",
        textAlign:"center",
    })
    thirdBannerView.add(url);
    
    var bannerView=Ti.UI.createScrollableView({
        width:VARS._dpiWidth+"dp",
        height:viewHeight,
        top:"0dp",
        showPagingControl:true,
        pagingControlOnTop:false,
        pagingControlColor:"#00000000",
    })
    bannerView.addView(firstBannerView);
    if(p.user.description.length>0)
    {
        bannerView.addView(secondBannerView);
    }
    
    bannerView.addView(thirdBannerView);
    mainHolder.add(bannerView);
    
    //########### EVENT LISTENR
    bannerView.addEventListener('scroll',function(e){
        blackCover.opacity=e.currentPageAsFloat;
    })
    url.addEventListener('click',function(e){
        Titanium.Platform.openURL(p.user.url);
    })
    profileImage.fireEvent('focus',{});
    return mainHolder;
    
}
exports.userBannerCreator=userBannerCreator;


var smartButton=function(p){
	return GUI.smartButton(p);	
}

exports.createLoadMoreButton=function(p){
	return GUI.createLoadMoreButton(p);
}


