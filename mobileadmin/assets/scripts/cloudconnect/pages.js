/**
 * Retreives list of Pages and return the Pages only 
 * @param {Object} p
 * @config {Function} listener, Listener for the result
 */
function retreivePages(p)
{
	sdk.sendRequest('objects/page/query.json', 'GET', {response_json_depth:1,page:1,per_page:50}, function(responseData) {
		p.listener(responseData.response.page);
	});
}

/**
 * @param {Object} p
 * @config {String} name, name of the page
 * @config {Function}, Listener for the correct result
 */
function addpage(p)
{
	var fields={title:p.title,lang:p.lang,content:p.content};
	//console.dir(fields);
	sdk.sendRequest('objects/page/create.json', 'POST', {fields:JSON.stringify(fields)}, function(responseData) {
		p.listener(responseData);
	});
}

/**
 * @param {Object} p
 * @config {String} title, title of the page
 * @config {Number} id, the id of the page
 * @config {String} content, content of the page
 * @config {Function}, Listener for the correct result
 */
function updatepage(p)
{
	var fields={title:p.title,lang:p.lang,content:p.content};
	console.dir(fields);
	sdk.sendRequest('objects/page/update.json', 'PUT', {id:p.id,fields:JSON.stringify(fields)}, function(responseData) {
		p.listener(responseData);
	});
}

/**
 * @param {Object} p
 * @config {Number} id, the id of the page
 * @config {Function}, Listener for the correct result
 */
function deletepage(p)
{
	var fields={name:p.name};
	sdk.sendRequest('objects/page/delete.json', 'DELETE', {id:p.id}, function(responseData) {
		p.listener(responseData);
	});
}

/**
 * Sets the Pages to the drop down of Pages 
 * @param {Object} p
 */
function setPagesToElement(p)
{
	retreivePages({
		listener:function(Pages)
		{
			var options = '';
      		for (var i = 0; i < Pages.length; i++) 
      		{
      			if(Pages[i].lang==p.lang)
      			{
      				options += '<option value="' + Pages[i].id + '">' + Pages[i].name + '</option>';
      			}
        		
     		 }
      		p.element.html(options);
      		p.listener(Pages);
      		return Pages;

		}
	})
}
