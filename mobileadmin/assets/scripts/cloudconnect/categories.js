/**
 * Retreives list of categories and return the categories only 
 * @param {Object} p
 * @config {Function} listener, Listener for the result
 */
function retreiveCategories(p)
{
	sdk.sendRequest('objects/category/query.json', 'GET', {response_json_depth:1,page:1,per_page:50}, function(responseData) {
		p.listener(responseData.response.category);
	});
}

/**
 * @param {Object} p
 * @config {String} name, name of the category
 * @config {Function}, Listener for the correct result
 */
function addCategory(p)
{
	var fields={name:p.name,lang:p.lang};
	sdk.sendRequest('objects/category/create.json', 'POST', {fields:JSON.stringify(fields)}, function(responseData) {
		p.listener(responseData);
	});
}

/**
 * @param {Object} p
 * @config {String} name, name of the category
 * @config {Number} id, the id of the category
 * @config {Function}, Listener for the correct result
 */
function updateCategory(p)
{
	var fields={name:p.name,lang:p.lang};
	sdk.sendRequest('objects/category/update.json', 'PUT', {id:p.id,fields:JSON.stringify(fields)}, function(responseData) {
		p.listener(responseData);
	});
}

/**
 * @param {Object} p
 * @config {Number} id, the id of the category
 * @config {Function}, Listener for the correct result
 */
function deleteCategory(p)
{
	var fields={name:p.name};
	sdk.sendRequest('objects/category/delete.json', 'DELETE', {id:p.id}, function(responseData) {
		p.listener(responseData);
	});
}

/**
 * Sets the categories to the drop down of categories 
 * @param {Object} p
 */
function setCategoriesToElement(p)
{
	retreiveCategories({
		listener:function(categories)
		{
			var options = '';
      		for (var i = 0; i < categories.length; i++) 
      		{
      			if(categories[i].lang==p.lang)
      			{
      				options += '<option value="' + categories[i].id + '">' + categories[i].name + '</option>';
      			}
        		
     		 }
      		p.element.html(options);
      		p.listener(categories);
      		return categories;

		}
	})
}
