function ApplicationTabGroup(sections) {
	//create module instance
	var self = Ti.UI.createTabGroup();
	self.tabsTintColor=VARS.barColor;
	
	for(var i=0;i<sections.length;i++)
	{
		var theTab=Ti.UI.createTab({
			title: sections[i].title,
			icon: "/images/tab/"+sections[i].icon,
			window: sections[i].win,
		});
		self.addTab(theTab);
		
		sections[i].win.containingTab = theTab;
	}
	
	return self;
};

module.exports = ApplicationTabGroup;
