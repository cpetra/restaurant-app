//################# STATIC DATA - DO NOT CHANGE #####################
exports._NEXT_GEN="nextgen";
exports._FB_GALLERY="fb";
exports._PICASA="pic";
exports._FLICKR="flic";
exports._FB="fb";
exports._RSS="rss";
exports._ACS="acs";
exports._WP="wp";
exports._TW="tw";

exports._iPad="ipad";
exports._iPhone="iphone";
exports._android="android";

exports._platform=Ti.Platform.osname;
exports._iOS=(Ti.Platform.osname=="ipad"||Ti.Platform.osname=="iphone");

//################# PROPERTIES GET - DO NOT CHANGE ################
var settings=Ti.App.Properties.getObject('settings',null);

getProperty=function(name,oClass){
    if(settings&&settings[oClass]){
        return settings[oClass][name];
    }else{
        return null;
    }
}


//################# USER DATA - CHANGES HERE ######################

//Navigation Settings
var navigationType=getProperty('navigation','config')||"slider" // Possible:  tab, metro, slider

//Settings for slider menu
exports.sliderMenuRows=getProperty('sliderRows','config')||2;
exports.sliderMenuColumns=getProperty('sliderColumns','config')||3;

//CHANGE THE TYPE OF GALLERY HERE
exports._typeOfGallery=getProperty('type','gallery')||exports._FLICKR;  // Possible Values:  exports._FB_GALLERY, exports._NEXT_GEN, exports._PICASA, exports._FLICKR

//How long the connection to api should take
exports._timeout=10000;

//Other settings
exports._AppName=getProperty('appname','config')||"My App";


//Settings for facebook
var fb = require('facebook');
fb.appid = getProperty('facebookAppId','config')||'< YOUR FACEBOOK APP ID >';
fb.permissions = ['publish_stream', 'read_stream'];;
fb.forceDialogAuth = true;
exports.fbappsecret=getProperty('facebookAppSecret','config')||"< YOUR FACEBOOK APP SECRET >";  // Can be found in https://developers.facebook.com/apps,, Without this FB gallery will not work


//Menu Settings
exports._receiverEmail=getProperty('orderemail','payment') || getProperty('orderemail','config')||"orderreceiver@example.com";
var paymentType=getProperty('payment','payment')||3;
exports.allowOrdering=paymentType!=4; //true or false
exports.allowPayPalPayment=paymentType==1||paymentType==2?true:false;
exports.allowMenuSubmiting=paymentType==3||paymentType==2?true:false;
exports.tax=parseFloat(getProperty('tax','payment')||0);
exports.paypaltype=getProperty('paypaltype','payment')||1;
exports.paypalappid=getProperty('paypalappid','payment')||null;
exports.paypalcurrency=getProperty('currency','payment')||"USD";
exports.paypalemail=getProperty('paypalemail','payment')||"yourpaypalemail@yahoo.com";
exports.ipnurl=getProperty('ipnurl','payment')||"";

//###################### Gallery Settings ##############
exports._NextGen_Address=getProperty('nextGenAdress','gallery')||"";  //Link to your wordpress blog
exports._PicasaUserId=getProperty('picasaUserId','gallery')||"";   //FIND your picasa user id and enter it here
exports._FlickrAppKEY=getProperty('flickerAppKey','gallery')||"1e41628a60f96bec1d00c9c6453f9ebf"; // REGISTER for your own Flick APP ID, This will be functional for short period of time
exports._FlickrUserID=getProperty('flickerUserId','gallery')||"52617155@N08" //FIND YOUR FLICKR USER ID ---> http://idgettr.com/
exports._FBPageID=getProperty('facebookPageId','gallery')||'419138831448318';  //Replace it with your own facebook page id or page name
exports._TIMTHUMB_FOR_GALLERY=getProperty('facebookTimthumb','gallery')||""; //Location to Titmthum used for gallery -- READ DOCS FOR MORE INFO. 
exports._TIMTHUMB_FACEBOOK_ENABLED=true; // Set this to true, if you timthumb  accept facebook ( Read documentation )
exports._IMAGE_QUALITY="100"; //Set lower number if image loading is slow

exports._DisplayDescription=true;  //true / false  to display description in the image if it exists 
exports._PhotoLimitFacebook=30; //Change the value here how many photos you want to fetch


//####################### LINKS ########################
exports.contactLogoBackground="#00FFFFFF";
exports.contactLogoImage="/images/logo.png";
exports.contactListData=[
    [
        {
            "value": "Facebook",
            "name": "Title"
        },
        {
            "value": getProperty('facebookName','contact')|| "fb.com\NextWebArt",
            "name": "SubTitle"
        },
        {
            "value": getProperty('facebooklink','contact')|| "http://www.facebook.com/NextWebArt",
            "name": "Value"
        },
        {
            "value": "#00547bbc",
            "name": "List Color"
        },
        {
            "value": "Facebook",
            "name": "Type",
        }
    ],
    [
        {
            "value": "Phone",
            "name": "Title"
        },
        {
            "value": getProperty('phone','contact')||"+38978000000",
            "name": "SubTitle"
        },
        {
            "value": getProperty('phone','contact') ? "tel:"+(getProperty('phone','contact').replace(/ /g,'')) : "tel:0038978000000",
            "name": "Value"
        },
        {
            "value": "#00e44690",
            "name": "List Color"
        },
        {
            "value": "Phone",
            "name": "Type",
        }
    ],
    [
        {
            "value": "E-Mail",
            "name": "Title"
        },
        {
            "value": getProperty('email','contact')||"contact@nextwebart.com",
            "name": "SubTitle"
        },
        {
            "value": getProperty('email','contact') ? "mailto:"+getProperty('email','contact') :"mailto:contact@nextwebart.com",
            "name": "Value"
        },
        {
            "value": "#0094ce46",
            "name": "List Color"
        },
        {
            "value": "Email",
            "name": "Type",
        }
    ],
    [
        {
            "value": "WebSite",
            "name": "Title"
        },
        {
            "value": getProperty('website','contact')||"nextwebart.com",
            "name": "SubTitle"
        },
        {
            "value": getProperty('website','contact')||"http://www.nextwebart.com",
            "name": "Value"
        },
        {
            "value": "#004a4a4a",
            "name": "List Color"
        },
        {
            "value": "WebLink",
            "name": "Type",
        }
    ],
    [
        {
            "value": "Location",
            "name": "Title"
        },
        {
            "value": getProperty('adress','contact')||"Skopje, Macedonia",
            
            "name": "SubTitle"
        },
        {
            "value": getProperty('adress','contact')?"https://www.google.it/maps?q="+getProperty('adress','contact'):"https://www.google.it/maps?q=Skopje",
            "name": "Value"
        },
        {
            "value": "#00547bbc",
            "name": "List Color"
        },
        {
            "value": "Map",
            "name": "Type",
        }
    ]
];

//######## LOCATIONS
exports._MapType=null;  
exports._MapLocations=[
    {
        title:getProperty('locationName','location')||"My Restaurant",
        latlon:getProperty('locationlatLng','location')||"40.7142,-74.0064",
        subTitle:getProperty('locationSubTitle','location')||"New York",
    }]

//Web Site
exports._hasWebPage=getProperty('website','about')!=null&&getProperty('website','about')!="";
exports._About_URL=getProperty('website','about')||"http://www.nextwebart.com/"; //Replace this with your own link, like info about your app, or ink to your facebook / twitter portfolio
exports._AboutContent=getProperty('content','about')||"";

//PUSH Notification for iPhone
exports.URBAN_AIRSHIP_APP_KEY=getProperty('appKey','airship')||"";
exports.URBAN_AIRSHIP_MASTER_SECRET=getProperty('masterSecret','airship')||"";
exports.useUrbanAirship=(exports.URBAN_AIRSHIP_APP_KEY!=""||exports.URBAN_AIRSHIP_APP_KEY!=null)&&(exports.URBAN_AIRSHIP_MASTER_SECRET!=""||exports.URBAN_AIRSHIP_MASTER_SECRET!=null);


//################# GLOBAL COLORS ###################
var templateName=getProperty('color','config')||'restaurant';
var Template=settings["colors"]||require('ui/templates/'+templateName); // Change with one of the templates.
exports.windowBackgroundColor=Template.windowBackgroundColor;
exports.barColor=Template.barColor;
exports.separatoColor=Template.separatoColor;
exports.rowColor=Template.rowColor;
exports.rowOddColor=Template.rowOddColor;
exports.textColor=Template.textColor;
exports.textSecondColor=Template.textSecondColor;
exports.listColors=Template.listColors || ["#fe4819","#d1005d","#0081ab","#009a3d"];

//Optional colors in the templates for videooo
exports.videosBackgroundColor=Template.videosWindowBackgroundColor||Template.windowBackgroundColor;
exports.videosBarColor=Template.videosCoversColor||Template.rowColor;
exports.videosTextColor=Template.videosTextColor||Template.textColor;

//Optional colors in th template for contact form
exports.fontColor=Template.fontColor||Template.textColor||"#ffffff";
exports.imageFrameColor=Template.imageFrameColor||Template.rowOddColor||"black";
exports.formTextColor=Template.formTextColor||Template.textColor||"white";
exports.formBgColor=Template.formBgColor||"white";


//Optionsal colors for the reloader in news section
exports.reloadBackgroundColor="#f0f0f0";
exports.reloadTextColor="#444444";
exports.buttonColor=Template.buttonColor||"#010101";
exports.buttonTopColor=Template.buttonTopColor||"#010101";
exports.buttonBottomColor=Template.buttonBottomColor||"#333333";

//IOS 7 colors
exports.tabTintColor="#FFFFFF";


//ADMOB ADS
exports.iphonePublisher_id=getProperty('adMobiPhone','config')||"";//YOUR iPhone PUBLISHER ID HERE  - Leave empty if you don't want to display ads 
exports.ipadPublisher_id=getProperty('adMobiPad','config')||"";//YOUR iPad PUBLISHER ID HERE 
exports.androidPublisher_id=getProperty('adMobAndroid','config')||""; //YOUR Android PUBLISHER ID HERE
exports.adTesting=true;//Set to false, when you publish the app

//#################### STOP CHANGES HERE ################ THE BELLOW CODE IS NOT RECOMENDED TO BE CHANGED
if(exports._platform==exports._iPad&&exports.ipadPublisher_id!=""){exports.displayAdMobAds=true}
if(exports._platform==exports._iPhone&&exports.iphonePublisher_id!=""){exports.displayAdMobAds=true}
if(exports._platform==exports._android&&exports.androidPublisher_id!=""){exports.displayAdMobAds=true}

exports.displayAdMobAdsNavigation=(exports.displayAdMobAds&&true); //Change from true to false to hide the ads in navigation
exports.displayAdMobAdsNews=(exports.displayAdMobAds&&true); //Change from true to false to hide the ads in news
exports.displayAdMobAdsVideos=(exports.displayAdMobAds&&true); //Change from true to false to hide the ads in videos
exports.displayAdMobAdsLink=(exports.displayAdMobAds&&true);
exports.displayAdMobAdsNavigation=(exports.displayAdMobAds&&true);


var iphoneAdHeight=50;
var iphoneAdWidth=320;
var androidAdHeight=50;
var androidAdWidth=320;
var ipadAdHeight=90;
var ipadAdWidth=728;
exports.AdHeight=iphoneAdHeight;
exports.AdWidth=iphoneAdWidth; 
if(exports._platform==exports._iPad){
	exports.AdHeight=ipadAdHeight
	exports.AdWidth=ipadAdWidth}
if(exports._platform==exports._android){
	exports.AdHeight=androidAdHeight+"dp",
	exports.AdWidth=androidAdWidth+"dp";
}

exports.AdHeightDisplayed=exports.AdHeight;
exports.AdWidthDisplayed=exports.AdWidth; 
if(navigationType=="tab"&&exports._iOS)
{
	exports.AdHeightDisplayed+=50; //Because tabs has some width
}






//################# TEXT FORMATS ###################
exports.h1 = {
    fontSize : "18dp",
    fontStyle : 'bold',
    fontWeight : 'bold'
}
exports.h2 = {
    fontSize : "15dp",
    fontStyle : 'bold',
    fontWeight : 'bold'
}
exports.h3 = {
    fontSize : "12dp",
    fontStyle : 'bold',
    fontWeight : 'bold'
}
exports.h4 = {
    fontSize : "13dp"
};
exports.normal = {
    fontSize : "11dp"
};

exports.empahasys = {
    fontSize : "10dp",
    fontStyle : 'italic'
}

//################ DISPLAY SETTINGS ################
exports._dpiWidth=320; //Standard for iPhone and most of android devices
exports._dpiHeight=480-60; //Standard for iPhone <=4s  //For the nav bar
//But is is not same in some andorid Devices that have XHDI like Samsung S3 and HTC ONE X
if(Ti.Platform.osname=="android")
{
	exports._dpiWidth=(Ti.Platform.displayCaps.platformWidth/Ti.Platform.displayCaps.logicalDensityFactor);
	exports._dpiHeight=(Ti.Platform.displayCaps.platformHeight/Ti.Platform.displayCaps.logicalDensityFactor)-50;
	if(navigationType=="tab"){
        exports._dpiHeight=(Ti.Platform.displayCaps.platformHeight/Ti.Platform.displayCaps.logicalDensityFactor)-110;
    }else if(navigationType=="metro"){
        exports._dpiHeight=(Ti.Platform.displayCaps.platformHeight/Ti.Platform.displayCaps.logicalDensityFactor)-25;
    }
}

if(exports._platform==exports._iPhone)
{
	exports._dpiHeight=(Ti.Platform.displayCaps.platformHeight)-65; //-65 for navigation
}

//Recalculate desity for iPad
if(Ti.Platform.osname=="ipad")
{
	exports._dpiWidth=768;
	exports._dpiHeight=(Ti.Platform.displayCaps.platformHeight)-65; //-65 for navigation
}

//Recalculate desity for iPhone 5 or iPhone 5s
if(Ti.Platform.osname=="iphone"&&Ti.Platform.displayCaps.platformHeight>480)
{
	exports._dpiHeight=Ti.Platform.displayCaps.platformHeight-60; 
}


//Additional Exports
exports.androidAdHeight=androidAdHeight;
exports.androidAdWidth=androidAdWidth;
exports._NavigationType=navigationType;
exports.getProperty=getProperty;