/**
 * @param {Object} p
 */
function retreiveUsers(p)
{
	sdk.sendRequest('users/query.json', 'GET', {limit:400,order:'-created_at'}, function(responseData) {
		p.listener(responseData.response.users);
	});
}

/**
 * @param {Object} p
 * @config {Number} page
 */
function retreiveUsersOld(p)
{
	sdk.sendRequest('users/query.json', 'GET', {page:p.page,order:'-created_at'}, function(responseData) {
		p.listener(responseData);
	});
}

/**
 * @param {Object} p
 * @config {Number} user_id
 */
function deleteUser(p)
{
	sdk.sendRequest('users/delete.json', 'DELETE', {user_id:p.user_id}, function(responseData) {
		p.listener(responseData);
	});
}

/**
 * @param {Object} p
 * @config {Number} user_id
 */
function activateUser(p)
{
	sdk.sendRequest('users/update.json', 'PUT', {custom_fields:{active:"yes"}}, function(responseData) {
		p.listener(responseData);
	});
}
