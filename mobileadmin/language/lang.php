<?php

//Load the appropriate language
include_once("language/".$lang.".php");

function ___($string){
	global $strings;
	if(isset($strings[$string])){
		return $strings[$string];
	}else{
		return $string;
	}
	
}

function __($string){
	echo  ___($string);
}

?>