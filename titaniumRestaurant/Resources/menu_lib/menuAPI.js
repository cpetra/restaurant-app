/**
 * Menu Module for Mobidonia
 * @author Daniel Dimov, <dimovdaniel@yahoo.com>
 */

/**
 * @property Cloud
 * @type Titanium.Cloud
 */
var Cloud = require('ti.cloud');
Cloud.debug = true;

/**
 * @property VARS
 * @type Settings
 */
var VARS =require('common/globals');

/**
 * @method fetchCloudMenuCategories
 * Fetch categories from Ti Cloud
 * @param {Object} param
 * - {Function} listener Listener for the result
 * @return ArrayList of categories
 */
fetchCloudMenuCategories = function(param) {
    //List of categories
    var categories = [];




/*
    Cloud.Objects.query({
        classname : 'menucategory',
        page : 1,
        per_page : 100,
        "order" : "-updated_at"
    }, function(e) {
        if (e.success) {
            for (var i = 0; i < e.menucategory.length; i++) {
                var category = e.menucategory[i];
                categories.push({
                    id : category.id,
                    title : category.title,
                    photo : (category.photo && category.photo.urls && category.photo.urls.original) ? category.photo.urls.original : null,
                    index : i,
                    type : VARS._ACS,
                });
            }
            //Get category ordering
            var categoryOrdering = VARS.getProperty('category', 'categorypositions');
            if (categoryOrdering) {
                //Joint the sections
                for (var i = 0; i < categoryOrdering.length; i++) {
                    for (var j = 0; j < categories.length; j++) {
                        if (categoryOrdering[i].id == categories[j].id) {
                            categoryOrdering[i] = categories[j];
                             categories[j].used=true;
                            continue;
                        }
                    }
                }

                //Fix empty sections(Prevents bugs)
                for (var i = 0; i < categoryOrdering.length; i++) {
                    if (!categoryOrdering[i].title) {
                        //If sections doesn't have window, remove it.
                        categoryOrdering.splice(i, 1);
                    }
                }
                
                //Add categories that are not included in the menu
                for (var j = 0; j < categories.length; j++) {
                        if (!categories[j].used) {
                            categoryOrdering.push(categories[j]);
                        }
                    }
                categories = categoryOrdering;

            } 
            param.listener(categories);
            

        } else {
            param.listener([]);
        }
    });
};
    
   */ 
exports.fetchCloudMenuCategories=fetchCloudMenuCategories;

/**
 * @method fetchCloudMenu
 * Fetch menu from Ti Cloud
 * @param {Object} param
 * - {Function} listener Listener for the result
 * - {String} categoryID
 * @return ArrayList of menu items
 */
fetchCloudMenu = function(param) {
    //List of menu items
    var items = [];

    Cloud.Objects.query({
        classname : 'menu',
        page : 1,
        per_page : 300,
        "order": "level,-updated_at",
        where :{
            category:param.categoryID,
        }
    }, function(e) {
        if (e.success) {
            for (var i = 0; i < e.menu.length; i++) {
                var item = e.menu[i];
                items.push({
                    id : item.id,
                    title : item.title,
                    intro : item.intro,
                    content : item.content,
                    prices : item.prices,
                    photo : (item.photo && item.photo.urls && item.photo.urls.original) ? item.photo.urls.original : null,
                    thumb: (item.photo && item.photo.urls && item.photo.urls.small_240) ? item.photo.urls.small_240 : null,
                    index : i,
                    type : VARS._ACS,
                });
            }
            //Get category ordering
            var itemsOrdering = VARS.getProperty(param.categoryID, 'resmenupositions');
            if (itemsOrdering) {
                //Joint the sections
                for (var i = 0; i < itemsOrdering.length; i++) {
                    for (var j = 0; j < items.length; j++) {
                        if (itemsOrdering[i].id == items[j].id) {
                            itemsOrdering[i] = items[j];
                             items[j].used=true;
                            continue;
                        }
                    }
                }

                //Fix empty sections(Prevents bugs)
                for (var i = 0; i < itemsOrdering.length; i++) {
                    if (!itemsOrdering[i].title) {
                        //If sections doesn't have window, remove it.
                        itemsOrdering.splice(i, 1);
                    }
                }
                
                //Add categories that are not included in the menu
                for (var j = 0; j < items.length; j++) {
                        if (!items[j].used) {
                            itemsOrdering.push(categories[j]);
                        }
                    }
                items = itemsOrdering;

            } 
            param.listener(items);
        } else {
            param.listener([]);
        }
    });
}
exports.fetchCloudMenu=fetchCloudMenu;


/**
 * @method submitOrder
 * Submit order to acs
 * @param {Object} param
 * - {Function} listener Listener for the result
 * - {Object} order
 */
submitOrder = function(param) {
    Cloud.Objects.create({
        classname : 'menuorder',
        fields :param.order
    }, function(e) {
        if (e.success) {
            var menuorder = e.menuorder[0];
            Ti.API.info("ID:"+menuorder.id);           
        } else {
            Ti.API.info('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
        }
        
    });
}
exports.submitOrder=submitOrder;


