<!DOCTYPE html>
<html>
<?php
   include 'static/vars.php';
   include_once("language/lang.php");
   $sm="2;4"; //Selected menu
?>
	<head>
	<title><?php __('order_details'); ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="description" content="">
	<meta name="keywords" content="admin, bootstrap,admin template, bootstrap admin, simple, awesome">
	<meta name="author" content="">

	<!-- Bootstrap -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/third/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/style-responsive.css" rel="stylesheet">
	<link href="assets/css/animate.css" rel="stylesheet">
	<link href="assets/third/morris/morris.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/third/nifty-modal/css/component.css">
	<link rel="stylesheet" href="assets/third/sortable/sortable-theme-bootstrap.css"> 
	<link rel="stylesheet" href="assets/third/icheck/skins/minimal/grey.css"> 
	<link rel="stylesheet" href="assets/third/select/bootstrap-select.min.css"> 
	<link rel="stylesheet" href="assets/third/summernote/summernote.css" />
	<link rel="stylesheet" href="assets/third/magnific-popup/magnific-popup.css"> 
	<link rel="stylesheet" href="assets/third/pace/pace-theme-minimal.css">
	<link rel="stylesheet" href="assets/third/datepicker/css/datepicker.css"/>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="assets/img/favicon.ico">
	</head>
	<body class="tooltips">
	
	
	<!-- Begin page -->
	<div class="container">
		<!-- Modal Dialog -->
		<!-- Start sidebar menu -->
		<!-- Start right content -->
		<?php
		    include_once("static/modal.php");
   			include_once("static/navigation.php");
   			include_once("static/topbar.php");
		?>
		<!-- End of sidebar menu  and top bar-->
			
			
			
			<!-- ============================================================== -->
			<!-- Start Content here -->
			<!-- ============================================================== -->
            <div class="body content rows scroll-y">
				
				<!-- Page header -->
				<div class="page-heading animated fadeInDownBig">
					<h1><?php __('order_details'); ?></h1>
				</div>
				<!-- End page header -->
				
				
				<!-- Your awesome content goes here -->
				<div id="section-to-print" class="box-info animated fadeInDown section-to-print">
					<h2><strong><?php __('order'); ?> <span class="text-primary orderid">#</span></strong></h2>
					<div class="icon-print"><a onClick="window.print();" data-toggle="tooltip" title="Print" href="#"><i class="fa fa-print"></i></a></div>
					
					<div class="row">
						<div class="col-sm-4">
							
							<div class="company-column">
							<h4><img src="assets/img/logo.png" class="img-circle" alt="Logo"> <strong class="bussiness_name"></strong></h4>
								<address>
								  <span class="bussiness_name"></span><br>
								   <span class="bussiness_adress"></span><br>
								  <abbr title="Phone"><span class="bussiness_phone"></span><br>
								</address>

								<address>
								  <strong><?php __('email'); ?></strong><br>
								  <a href="#"><span class="bussiness_email"></span><br></a>
								</address>
							</div>
							
						</div>
						<div class="col-sm-4">
							
							<div class="company-column">
							<h4><strong><?php __('ship_to'); ?></strong>:</h4>
									<address>
									  <strong id="user_name"></strong><br>
									  <span id="user_address"></span><br />
									  <abbr title="Phone">P:</abbr> <span id="user_phone"></span><br />
									  <?php __('note'); ?><span id="user_note"></span><br />
									</address>

									
							</div>
							
						</div>
						<div class="col-sm-4">
							<p class="text-right"><strong><?php echo date("j F Y H:i"); ?></strong></p>
						</div>
					</div>
					
					<p><strong><?php __('order_date'); ?>   : </strong><span id="date"></span></p>
					<p><strong><?php __('order_status'); ?>   : </strong><span id="status"><span class="label label-warning">Pending</span></span></p>
					<p><strong><?php __('order_id'); ?>   : </strong>#<span class="orderid"></span></p>
					
					<hr />
					
					<div class="table-responsive">
						
						<table class="table">
							<thead>
								<tr>
									<th><?php __('items'); ?></th>
									<th><?php __('qty'); ?></th>
									<th><?php __('type'); ?></th>
									<th><?php __('unit_price');?></th>
									<th><?php __('total'); ?></th>
								</tr>
							</thead>
							
							<tbody>
							</tbody>
							
						</table>
					
					</div>


					
				</div>
				<!-- End of your awesome content -->
							  <button type="button" id="buttonProcessing" class="btn btn-info"><?php __('processing'); ?></button>
							  <button type="button" id="buttonDelivered" class="btn btn-success"><?php __('delivered'); ?></button>
							  <button type="button" id="buttonNewOrder" class="btn btn-warning"><?php __('new_order'); ?></button>
							  <button type="button" id="buttonSuspended" class="btn btn-danger"><?php __('suspended'); ?></button>
							  <img  class="loader_save_update" src="assets/img/ajax-loader.gif" />
			
            </div>
			<!-- ============================================================== -->
			<!-- End content here -->
			<!-- ============================================================== -->	
        </div>
		<!-- End right content -->
		
		
		
		
		
		<!-- the overlay modal element -->
		<div class="md-overlay"></div>
		<!-- End of eoverlay modal -->
		
		
		
	</div>
	<!-- End of page -->

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="assets/js/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/third/knob/jquery.knob.js"></script>
		<script src="assets/third/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="assets/third/morris/morris.js"></script>
		<script src="assets/third/nifty-modal/js/classie.js"></script>
		<script src="assets/third/nifty-modal/js/modalEffects.js"></script>
		<script src="assets/third/sortable/sortable.min.js"></script>
		<script src="assets/third/select/bootstrap-select.min.js"></script>
		<script src="assets/third/summernote/summernote.js"></script>
		<script src="assets/third/magnific-popup/jquery.magnific-popup.min.js"></script> 
		<script src="assets/third/pace/pace.min.js"></script>
		<script src="assets/third/input/bootstrap.file-input.js"></script>
		<script src="assets/third/datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/third/icheck/icheck.min.js"></script>
		<script src="assets/third/wizard/jquery.easyWizard.js"></script>
		<script src="assets/third/wizard/scripts.js"></script>
		<script src="assets/js/lanceng.js"></script>
		<script src="assets/js/moments.js"></script>

		<!-- Cloud connect scripts -->
		<script type="text/javascript" src="assets/scripts/cocoafish-1.2.js"></script> 
    	<script type="text/javascript" src="assets/scripts/utils.js"></script>
    	<script type="text/javascript" src="assets/scripts/cloudconnect/objects.js"></script>
    	<script>



 var editId=null;
 var menus=[];
 var categories=[];
 var currency="$";
 var per_page=4;
 var order_id="<?php echo $_GET["id"]; ?>";
 var totalValue=0;
 var theOrder=null;

 var settings={payment:{tax:0}};
  function fetchAndUpdateSettings(){
  	$('.loader_save_update').show();
   retreiveObjects({
    objectClass:"settings",
    query:{response_json_depth:1,page:1,per_page:50},
    listener:function(data){
      setUpSettings({
        items:data,
      })
    }
  })
 };


 function fetchAndUpdateList(page){
 	$('.loader').show(); //Show the loading indicator

 	retreiveObjectsFull({
 		objectClass:"menuorder",
 		query:{
 			per_page:per_page,
 			page:page,
 			where: JSON.stringify({
        		id:order_id,
    		})
 			
 		},
 		listener:function(data){
 			fetchAndUpdateSettings();
 			setUpCategories(data)
 		}
 	})
 };



 function restartForm(){
 	editId=null;
 	$('#price_holder').html("");
 	document.getElementById("add_post").reset();
         $('#textArea').val("");
     }

     function createDate(dat){
     	var a = dat.split(/[^0-9]/);
        var d1=new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
     	return '<td>'+d1.toDateString()+' '+d1.getHours()+':'+d1.getMinutes()+'</td>';
     }

     function createStatus(status){
     	typeStatus="warning" //warning //danger //success
     	label="<?php __('new_order'); ?>";
     	if(status==1){
     		typeStatus="info"
     		label="<?php __('processing'); ?>";
     	}else if(status==2){
     		typeStatus="success"
     		label="<?php __('delivered'); ?>";
     	} else if(status==3){
     		typeStatus="danger"
     		label="<?php __('suspended'); ?>";
     	}
     	return '<td><span class="label label-'+typeStatus+'">'+label+'</span></td>'
     }


     function createSingleRow(product){
     		var ret='<tr>';
									ret+='<td>'+product.title+'</td>';
									ret+='<td>'+product.selectedQty+'</td>';
									ret+='<td>'+product.prices[product.selectedIndex].name+'</td>';
									ret+='<td><span class="curr"></span> '+product.prices[product.selectedIndex].value+'</td>';
									itemValue=product.prices[product.selectedIndex].value*product.selectedQty;
									totalValue+=itemValue;
									ret+='<td><span class="curr"></span> '+itemValue+'</td>';								
								ret+='</tr>';
			return ret;
     }

   

     function createPagnitation(numPages,current){
     	var paging="";
     	for(var i=0; i<numPages;i++){
     		paging+='<li class="'+(current==(i+1)?"active":"")+'"><a href="#" onClick="fetchAndUpdateList('+(i+1)+')">'+(i+1)+'</a></li>';
     	}
     	return paging;
     }

     function addTotalAndTaxRow(){
     	//Create tax row;
		var theTax=(totalValue/100)*settings.payment.tax;
		var withTax=totalValue+theTax;
     	var taxRow='<tr>';
									taxRow+='<td colspan="4"><p class="text-right"><strong><?php __("tax") ?>: </strong>'+settings.payment.tax+'%</p></td>';
									taxRow+='<td><span class="curr"></span> '+theTax+'</td>';
								taxRow+='</tr>';
		$('tbody').append(taxRow);

		//Create grand row;
     	var grandTotalRow='<tr>';
									grandTotalRow+='<td colspan="4"><p class="text-right"><strong><?php __("grand_total") ?>: </strong></p></td>';
									grandTotalRow+='<td><span class="curr"></span> '+withTax+'</td>';
								grandTotalRow+='</tr>';
		$('tbody').append(grandTotalRow);
     }

     function setUpCureenecyAndInfo(){
     	$('.curr').html(settings.config.currency);
     	$('.bussiness_name').html(settings.location.locationName);
     	$('.bussiness_phone').html(settings.contact.phone);
     	$('.bussiness_email').html(settings.contact.email);
     	$('.bussiness_adress').html(settings.contact.adress);
     	
     	
     }

      function setUpCategories(raw){

      	$('tbody').html(""); //Remove old rows
     	$('.loader').hide(); //Hide the loading indicator
     	data={items:raw.response['menuorder']};
     	
     	console.log("Num orders:"+data.items.length);
     	theOrder=data.items[0];
     	if(data.items.length==1){
     		order=data.items[0];
     	    $('#date').html(moment(order.created_at).format('MMMM Do YYYY, H:mm'));
     	    $('#status').html(createStatus(order.status));
     	     $('#user_name').html(order.name);
     	      $('#user_address').html(order.address);
     	       $('#user_phone').html(order.phone);
     	       $('#user_note').html(order.notes)
     	       $('.orderid').html(order.orderUniqueID);
     	}
     	for(var i=0;i<order.orderList.length;i++){
     		$('tbody').append(createSingleRow(order.orderList[i]));
     		console.log("Adding "+order.orderList[i].title);
     	}

     	//Create total row;
     	var totalRow='<tr>';
									totalRow+='<td colspan="4"><p class="text-right"><strong><?php __("total") ?></strong></p></td>';
									totalRow+='<td><span class="curr"></span> '+totalValue+'</td>';
								totalRow+='</tr>';
		$('tbody').append(totalRow);



 	}

 	function setUpSettings(data){
	$('.loader_save_update').hide();
 console.log(data.items.length);
 if(data.items.length>0){
  settings=data.items[0];

  if(settings.payment){   
     
  }
  
}else{
  settings={payment:{tax:0}};
}
addTotalAndTaxRow();
setUpCureenecyAndInfo();

};


    

     function saveMenu(){
     	console.log("Creating the menu item");
     	$('.loader_save_update').show();
     	var title=$('#title').val();
     	console.log("Add / Update category item id:"+editId+"  "+title);
     	


     	var topublish={
     		title:title,
     	}
     	console.log(topublish);
     	if(editId!=null){
            //We are doing update
            console.log("We are doing update to "+editId);
           updateClassObject({
            	objectClass:"menucategory",
            	fields:topublish,
            	id:editId,
            	photo:'image',
            	listener:function(e){
               window.location = 'menucategory.php';
           }
       });
            
        }else{
            
            	console.log("We are creating new menu item");
            	addClassObject({
            		objectClass:"menucategory",
            		fields:topublish,
            		photo:'image',
            		listener:function(e){
            			window.location = 'menucategory.php';
            		}
            	})
            
            
            
        }

    }

    function editMenu(index){
    	restartForm();
    	console.log("Edit index "+index);
    	$('#form_name').html('<?php __('update_category'); ?>');
    	$('#save_button').html('<?php __('update'); ?>');
    	var menu=menus[index];
    	editId=menu.id;
    	$('#title').val(menu.title);
    	if(menu.photo){
    		if( menu.photo.urls&& menu.photo.urls.medium_500){
    			$("#preview").attr('src', menu.photo.urls.medium_500);
    		}else{
    			alert('<?php __("image_notify"); ?>');
    		}

    	}

    	if(menu.prices){
    		for(var i=0;i<menu.prices.length;i++){
    			$('#price_holder').append(createAddonPrice(menu.prices[i].name,menu.prices[i].value));
    		}
    	}

         //Add prices
         
     }

     function startDeleteMenu(index){
     	console.log("Delete index "+index);

     	var menuId=menus[index].id;
     	deleteClassObject({
     		listener:function(e){fetchAndUpdateList(1);},
     		id:menuId,
     		objectClass:"menuorder"
     	})


     }

     function  deleteMenu(index) {


     		if (confirm("<?php __('sure_delete');?>") == false) {
     			return;
     		}

     		
     		startDeleteMenu(index); 

     	};

     function updateOrder(status){
     	$('.loader_save_update').show();
     	theOrder.status=status;
     	updateClassObject({
            	objectClass:"menuorder",
            	fields:theOrder,
            	id:order_id,
            	photo:null,
            	listener:function(e){
                window.location = 'menuorders.php';
           }
     });
     }


     

   
     jQuery(document).ready(function() {    
     	//$('.loader_save_update').hide();
     	fetchAndUpdateList();    
     	$('#buttonProcessing').click(function(e){
     		updateOrder(1);
     	});	
     	$('#buttonDelivered').click(function(e){
     		updateOrder(2);
     	});
     	$('#buttonSuspended').click(function(e){
     		updateOrder(3);
     	});
     	$('#buttonNewOrder').click(function(e){
     		updateOrder(0);
     	});

     });
</script>  
	</body>
</html>