/**
 * Retreives list of comments
 * @param {Object} p
 * @config {Function} listener, Listener for the result
 * @config {String} post_id, Id of the queried post
 * @config {String} custom_object_id, used for the costume objects
 * @config {String} deleteid, comment to be deleted
 */
function retreiveComments(p)
{
	if(p.deleteid!=0){
		console.log("Delete review id "+p.deleteid+" from costume object " +p.custom_object_id);
		deleteComment({
			custom_object_id:p.custom_object_id,
			review_id:p.deleteid,
			listener:function(e){},
		})
	}
	sdk.sendRequest('reviews/query.json', 'GET', {custom_object_id:p.custom_object_id,post_id:p.post_id}, function(responseData) {
		p.listener(responseData.response.reviews,p.custom_object_id,p.deleteid);
	});
}


/**
 * @param {Object} p
 * @config {String} post_id
 * @config {String} review_id
 * @config {String} custom_object_id
 */
function deleteComment(p)
{
	sdk.sendRequest('reviews/delete.json', 'DELETE', {custom_object_id:p.custom_object_id,post_id:p.post_id,review_id:p.review_id}, function(responseData) {
		p.listener(responseData);
	});
}