/**
 * @author Daniel Dimov - NextWebArt  - dimovdaniel@yahoo.com
 * Titanium Restaurant App
 */

//##################### DATA ##################
var VARS =require('common/globals'); //Here we store all global variables / settings
var Window = require('ui/handheld/ApplicationWindow');
var WebWindow = require('ui/handheld/ApplicationWebWindow');
var Gallery=require('galleries_lib/galleryModule'); //Here are the functions to create the gallery
var News=require('news_lib/newsModule'); //Here are the functions to create news
var Contact=require('contact_lib/contactModule'); //Here are the functions to create the links
var Menu=require('menu_lib/menuModule');
var Maps=require('maps_lib/mapsModule'); //Here are the functions to create the maps



/**
 * @property Cloud
 * @type Titanium.Cloud
 */
var Cloud = require('ti.cloud');
Cloud.debug = true;

//For iPhone only include the push module
if(VARS._platform==VARS._iPhone&&VARS.useUrbanAirship)
{
    var Push=require('push_lib/pushModule');
    Push.registerDisplayUrbanAirshipPush({
        URBAN_AIRSHIP_APP_KEY:VARS.URBAN_AIRSHIP_APP_KEY,
        URBAN_AIRSHIP_MASTER_SECRET:VARS.URBAN_AIRSHIP_MASTER_SECRET
    })
}


if(VARS._displayAdMobAds)
{
	var Admob = require('ti.admob');
}

//Indicator to see if script is initialized
var galleryInitialized=false;
var newsInitialized=false;
var videoInitialized=false;
var contactInitialized=false;
var linksInitialized=false;
var aboutInitialized=false;
var twitterInitialized=false;
var menuInitialized=false;
var mapInitialized=false;

var openType=1;

//bootstrap and check dependencies
if (Ti.version < 1.8 ) {
	alert('Sorry - this application template requires Titanium Mobile SDK 1.8 or later');
}


//################### VIEWS & WINDOW ################
var menuWindow = new Window(L('menu'));
var menuSection={
    win:menuWindow,
    icon:'menu.png',
    title:L('menu'),
    id:"nav_menu"
}

var galleryWindow = new Window(L('gallery'));
var gallerySection={
	win:galleryWindow,
	icon:'camera.png',
    title:L('gallery'),
    id:"nav_gallery"
}

var newsWindow = new Window(L('news'));
var newsSection={
	win:newsWindow,
	icon:'news.png',
    title:L('news'),
    id:"nav_news"
}

var linkWindow = new Window(L('contact'));
var linkSection={
	win:linkWindow,
	icon:'contact.png',
    title:L('contact'),
    id:"nav_contact_list"
}


var aboutWindow= new Window(L('web'));
var aboutSection={
	win:aboutWindow,
	icon:'info.png',
    title:L('info'),
    id:"nav_about"
}

var locationWindow = new Window(L('location'));
locationWindow.translucent=true;
locationWindow.extendEdges = [Ti.UI.EXTEND_EDGE_ALL];
var locationSection={
	win:locationWindow,
	icon:'map.png',
    title:L('location'),
    id:"nav_location",
}



//######################## SECTION ORDER - CHANGE HERE #################
var sections=[];
sections.push(menuSection);
sections.push(newsSection);
sections.push(gallerySection);
sections.push(linkSection);
sections.push(aboutSection);
sections.push(locationSection);

//######################## FUNCTION #####################################
reorderMenu=function(){
   var orderMenu=VARS.getProperty('available','navigation');   
   //Joint the sections
   for(var i=0;i<orderMenu.length;i++){
       for(var j=0;j<sections.length;j++){
           if(orderMenu[i].id==sections[j].id){
               orderMenu[i]=sections[j];
               continue;
           }
       }
   }
   
   //Fix empty sections(Prevents bugs)
   for(var i=0;i<orderMenu.length;i++){
       if(!orderMenu[i].win){
           //If sections doesn't have window, remove it.
           orderMenu.splice(i, 1);
       }
   }
   sections=orderMenu;
}
if(VARS.getProperty('available','navigation')){reorderMenu();}
//#################### MENU FUNCTION ############
createMenu=function(){
    Menu.createCategories({
        win:menuWindow,
        allowOrdering:VARS.allowOrdering
    });
}
//#################### CONTACT FUNCTION ###########
createContactList=function()
{
    Contact.createContactList({
        win:linkWindow,
        logoBackground:VARS.contactLogoBackground,
        logoImage:VARS.contactLogoImage,
        data:VARS.contactListData,
        });
}
//################ GALLERY FUNCTIONS ##############	
//Function to create NextGen Gallery
createNextGenGallery=function()
{
	
	Gallery.createGalleryList({
		win:galleryWindow,
		id:1,
		url:VARS._NextGen_Address,
		type:VARS._NEXT_GEN,
		openType: openType,
		//tab:tabGallery
		});
}

//Function to create Facebook Gallery
createFBGallery=function()
{

		Gallery.createGalleryList({
			win:galleryWindow,
			page:VARS._FBPageID,
			type:VARS._FB_GALLERY,
			openType: openType,
			//tab:tabGallery
		});
}

//Function to create Facebook Gallery
createPicasaGallery=function()
{
	
		Gallery.createGalleryList({
			win:galleryWindow,
			userID:VARS._PicasaUserId,
			type:VARS._PICASA,
			openType: openType,
			//tab:tabGallery
		});
}

//Function to create Flickr Gallery
createFlickrGallery=function()
{

		Gallery.createGalleryList({
			win:galleryWindow,
			type:VARS._FLICKR,
			openType: openType,
			//tab:tabGallery,
			appKEY:VARS._FlickrAppKEY,
			userID:VARS._FlickrUserID,
		});
}

//################ NEWS FUNCTIONS ################
//Function to create ACS news
createACSNews=function(){
    News.createNewsList({
            type:VARS._ACS,
            win:newsWindow,
        })
}
//Function to create RSS
createRSSNews=function()
{
	News.createNewsList({
			type:VARS._RSS,
			url:VARS._RSS_URl,
			win:newsWindow,
		})
}

//################ MAP ###########################
createSingleMap=function()
{
    Maps.createMap({
        win:locationWindow,
        locations:VARS._MapLocations,
        openType:1,
        type:VARS._DistanceType,
        mapType:VARS._MapType});
}
//################ WEB FUNCTIONS #################
//Function to create about web section
createAboutWeb=function()
{
	var webviewabout = Titanium.UI.createWebView({});
	if(VARS._hasWebPage){
	   webviewabout.setUrl(VARS._About_URL);
	}else{
	    webviewabout.setHtml(VARS._AboutContent)
	}
	aboutWindow.add(webviewabout);
}

//######################### EVENT LISTENERS #####################
//Function that listens for open event on the GalleryWindow
galleryWindow.addEventListener('open',function(e)
{
	if(!galleryInitialized)
	{
		if(VARS._typeOfGallery==VARS._NEXT_GEN)
		{
			createNextGenGallery();
		}
		else if(VARS._typeOfGallery==VARS._FB_GALLERY)
		{
			createFBGallery();
		}
		else if(VARS._typeOfGallery==VARS._PICASA)
		{
			createPicasaGallery();
		}
		else if(VARS._typeOfGallery==VARS._FLICKR)
		{
			createFlickrGallery();
		}
		galleryInitialized=true;
	}
})


//Function that listens for open event on the NewsWindow
newsWindow.addEventListener('open',function(e)
{
	if(!newsInitialized)
	{
		createACSNews();
		newsInitialized=true;
	}
})



//Function that listens for focus on contact window
linkWindow.addEventListener('focus',function(e)
{
	if(!linksInitialized)
	{
		createContactList();
		linksInitialized=true;
	}
})

//Function that listens for focus on about window
aboutWindow.addEventListener('focus',function(e)
{
	if(!aboutInitialized)
	{
		createAboutWeb();
		aboutInitialized=true;
	}
})

//Function that listens for focus on about window
menuWindow.addEventListener('focus',function(e)
{
    if(!menuInitialized)
    {
        createMenu();
        menuInitialized=true;
    }
})


locationWindow.addEventListener('focus',function(e)
{
    if(!mapInitialized)
    {
        createSingleMap();
        mapInitialized=true;
    }
})




//######################## INIT FUNCTION ###############################
// This is a single context application with mutliple windows in a stack
var init=function(){
	//determine platform and form factor and render approproate components
	var osname = Ti.Platform.osname,
		version = Ti.Platform.version,
		height = Ti.Platform.displayCaps.platformHeight,
		width = Ti.Platform.displayCaps.platformWidth;
	
	Ti.API.info("Height:"+height);
	Ti.API.info("\Width:"+width);
	//considering tablet to have one dimension over 900px - this is imperfect, so you should feel free to decide
	//yourself what you consider a tablet form factor for android
	var isTablet = osname === 'ipad' || (osname === 'android' && (width > 899 || height > 899));

    //Determine the Navigation type
    var navigationHandler=null;
    if(VARS._NavigationType=="tab")
    {
    	navigationHandler= require('ui/common/ApplicationTabGroup');
    }
    else if(VARS._NavigationType=="slider")
    {
    	navigationHandler= require('ui/common/ApplicationSlider');
    }
     else if(VARS._NavigationType=="metro")
    {
    	navigationHandler= require('ui/common/ApplicationMetro');
    }
     else
    {
    	alert("Please choice valid menu style: TAB, SLIDER or METRO");
    	return;
    }
	new navigationHandler(sections).open();
}
init();



//*********************  PUSH NOTIFICATIONS **************************/
    function subscribeToken(token) {
        Cloud.PushNotifications.subscribeToken({
            device_token : token,
            type : VARS._iOS ? "ios" : "android",
            channel : "news",
        }, function(e) {
            if (e.success) {
                Titanium.App.Properties.setBool("tokenSubscribed", true);
            }
        });
    }

    if (VARS._platform == VARS._android) {
        var CloudPush = require('ti.cloudpush');
        CloudPush.retrieveDeviceToken({
            success : function deviceTokenSuccess(e) {
                Ti.API.info('Device Token: ' + e.deviceToken);
                Ti.App.Properties.setString("tokenANDROID", e.deviceToken);
                if (!Titanium.App.Properties.getBool('tokenSubscribed', false)) {
                    subscribeToken(e.deviceToken);
                }
            },
            error : function deviceTokenError(e) {
                //alert('Failed to register for push! ' + e.error);
            }
        });
        CloudPush.enabled = true;
    } else if (VARS._platform == VARS._iPhone) {

        function tokenReceivedIOS(e) {
            Ti.App.Properties.setString("tokenIOS", e.deviceToken);
            if (!Titanium.App.Properties.getBool('tokenSubscribed', false)) {
                subscribeToken(e.deviceToken);
            }
        }

        function errorCallbackPush(e) {
            Ti.App.Properties.setString("tokenIOS", "");
        }

        function messageCallbackPush(thePush) {
        }

        /**
         * Register user for push notification
         * @param {Object} p
         */
        var registerForPushIOS = function(p) {
            Titanium.Network.registerForPushNotifications({
                types : [Titanium.Network.NOTIFICATION_TYPE_BADGE, Titanium.Network.NOTIFICATION_TYPE_ALERT, Titanium.Network.NOTIFICATION_TYPE_SOUND],
                success : tokenReceivedIOS,
                error : errorCallbackPush,
                callback : messageCallbackPush
            });
        }
        registerForPushIOS();

    }

    //********************* END PUSH NOTIFICATION ************************/
   
//********************** LOGIN USER FOR ORDERING **********************/ 
if (getProperty('orderuser','payment') && getProperty('orderpass','payment')) {
    //Login this static user
    Cloud.Users.login({
        login : getProperty('orderuser','payment'),
        password : getProperty('orderpass','payment')
    }, function(e) {
        if (e.success) {
            Ti.App.Properties.setBool("userLoggedIn",true);
            var user = e.users[0];
            Ti.API.info('Success:\n' + 'id: ' + user.id + '\n' + 'sessionId: ' + Cloud.sessionId + '\n' + 'first name: ' + user.first_name + '\n' + 'last name: ' + user.last_name);
        } else {
            Ti.App.Properties.setBool("userLoggedIn",false);
            Ti.API.info('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
        }
    });
}else{
    Ti.API.info("Used user")
    Ti.API.info(getProperty('orderuser','payment'));
    Ti.API.info(getProperty('orderpass','payment'));
}

