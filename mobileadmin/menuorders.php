<!DOCTYPE html>
<html>
<?php
   include 'static/vars.php';
   include_once("language/lang.php");
   $sm="2;4"; //Selected menu
?>
	<head>
	<title><?php __('menu_orders'); ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="description" content="">
	<meta name="keywords" content="admin, bootstrap,admin template, bootstrap admin, simple, awesome">
	<meta name="author" content="">

	<!-- Bootstrap -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/third/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/style-responsive.css" rel="stylesheet">
	<link href="assets/css/animate.css" rel="stylesheet">
	<link href="assets/third/morris/morris.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/third/nifty-modal/css/component.css">
	<link rel="stylesheet" href="assets/third/sortable/sortable-theme-bootstrap.css"> 
	<link rel="stylesheet" href="assets/third/icheck/skins/minimal/grey.css"> 
	<link rel="stylesheet" href="assets/third/select/bootstrap-select.min.css"> 
	<link rel="stylesheet" href="assets/third/summernote/summernote.css" />
	<link rel="stylesheet" href="assets/third/magnific-popup/magnific-popup.css"> 
	<link rel="stylesheet" href="assets/third/pace/pace-theme-minimal.css">
	<link rel="stylesheet" href="assets/third/datepicker/css/datepicker.css"/>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="assets/img/favicon.ico">
	</head>
	<body class="tooltips">
	
	
	<!-- Begin page -->
	<div class="container">
		<!-- Modal Dialog -->
		<!-- Start sidebar menu -->
		<!-- Start right content -->
		<?php
		    include_once("static/modal.php");
   			include_once("static/navigation.php");
   			include_once("static/topbar.php");
		?>
		<!-- End of sidebar menu  and top bar-->
			
			
			
			<!-- ============================================================== -->
			<!-- Start Content here -->
			<!-- ============================================================== -->
            <div class="body content rows scroll-y">
				
				<!-- Page header -->
				<div class="page-heading animated fadeInDownBig">
					<h1><?php __('menu_orders'); ?> <small><?php __('menu_ord_desc'); ?></small></h1>
				</div>
				<!-- End page header -->
				
				
				<!-- List of products -->
				<div class="col-md-12">
				<div class="box-info full">
					<h2><?php __('menu_ord_table_name'); ?></h2>
						
						<div class="data-table-toolbar">
							<div class="row">
								<div class="col-md-4">
									<img class="loader" src="assets/img/ajax-loader.gif" />
								</div>
								
							</div>
						</div>
						
					<div class="table-responsive">
						<table data-sortable class="table table-hover table-striped">
							<thead>
								<tr>
									<th><?php __('order_name'); ?></th>
									<th><?php __('adress'); ?></th>
									<th><?php __('status'); ?></th>
									<th><?php __('date'); ?></th>
									<th data-sortable="false"><?php __('options'); ?></th>
								</tr>
							</thead>
							
							<tbody>
								
								
							</tbody>
						</table>
					</div>
						
					<div class="data-table-toolbar">
						<ul class="pagination">
						 
						</ul>

					</div>
					<div></div>
				</div>
				</div>

				
						
					</div>
				
				<!-- End of your awesome content -->
			
			
            </div>
			<!-- ============================================================== -->
			<!-- End content here -->
			<!-- ============================================================== -->	
        </div>
		<!-- End right content -->
		
		
		
		
		
		<!-- the overlay modal element -->
		<div class="md-overlay"></div>
		<!-- End of eoverlay modal -->
		
		
		
	</div>
	<!-- End of page -->

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="assets/js/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/third/knob/jquery.knob.js"></script>
		<script src="assets/third/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="assets/third/morris/morris.js"></script>
		<script src="assets/third/nifty-modal/js/classie.js"></script>
		<script src="assets/third/nifty-modal/js/modalEffects.js"></script>
		<script src="assets/third/sortable/sortable.min.js"></script>
		<script src="assets/third/select/bootstrap-select.min.js"></script>
		<script src="assets/third/summernote/summernote.js"></script>
		<script src="assets/third/magnific-popup/jquery.magnific-popup.min.js"></script> 
		<script src="assets/third/pace/pace.min.js"></script>
		<script src="assets/third/input/bootstrap.file-input.js"></script>
		<script src="assets/third/datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/third/icheck/icheck.min.js"></script>
		<script src="assets/third/wizard/jquery.easyWizard.js"></script>
		<script src="assets/third/wizard/scripts.js"></script>
		<script src="assets/js/lanceng.js"></script>
		<script src="assets/js/moments.js"></script>

		<!-- Cloud connect scripts -->
		<script type="text/javascript" src="assets/scripts/cocoafish-1.2.js"></script> 
    	<script type="text/javascript" src="assets/scripts/utils.js"></script>    
    	<script type="text/javascript" src="assets/scripts/cloudconnect/objects.js"></script> 


    	<script>



 var editId=null;
 var menus=[];
 var categories=[];
 var currency="$";
 var per_page=10;

 function fetchAndUpdateList(page){
 	$('.loader').show(); //Show the loading indicator

 	retreiveObjectsFull({
 		objectClass:"menuorder",
 		query:{
 			per_page:per_page,
 			page:page
 		},
 		listener:function(data){
 			setUpCategories(data)
 		}
 	})
 };



 function restartForm(){
 	editId=null;
 	$('#price_holder').html("");
 	document.getElementById("add_post").reset();
         $('#textArea').val("");
     }

     

     function createStatus(status){
     	typeStatus="warning" //warning //danger //success
     	label="<?php __('new_order'); ?>";
     	if(status==1){
     		typeStatus="info"
     		label="<?php __('processing'); ?>";
     	}else if(status==2){
     		typeStatus="success"
     		label="<?php __('delivered'); ?>";
     	} else if(status==3){
     		typeStatus="danger"
     		label="<?php __('suspended'); ?>";
     	}
     	return '<td><span class="label label-'+typeStatus+'">'+label+'</span></td>'
     }


     function createSingleRow(product,index){
     		var ret='<tr>';
									ret+='<td>'+product.name+'</td>';
									ret+='<td>'+product.address+'</td>';
									ret+=createStatus(product.status);
									ret+='<td>'+moment(product.created_at).format('MMMM Do YYYY, H:mm')+'</td>';
									ret+='<td>';
										ret+='<div class="btn-group btn-group-xs">';
											//ret+='<a onClick="deleteMenu(\''+index+'\');" data-toggle="tooltip" title="<?php __('delete'); ?>" class="btn btn-default"><i class="fa fa-power-off"></i></a>';
											ret+='<a href="menuinvoice.php?id='+product.id+'" data-toggle="tooltip" title="<?php __('edit'); ?>" class="btn btn-default"><i class="fa fa-edit"></i></a>';
										ret+='</div>';
									ret+='</td>';
								ret+='</tr>';
			return ret;
     }

     function createPagnitation(numPages,current){
     	var paging="";
     	for(var i=0; i<numPages;i++){
     		paging+='<li class="'+(current==(i+1)?"active":"")+'"><a href="#" onClick="fetchAndUpdateList('+(i+1)+')">'+(i+1)+'</a></li>';
     	}
     	return paging;
     }

      function setUpCategories(raw){

      	$('tbody').html(""); //Remove old rows
     	$('.loader').hide(); //Hide the loading indicator
     	$('.pagination').html(createPagnitation(raw.meta.total_pages,raw.meta.page)); //Clear pagnitation
     	data={items:raw.response['menuorder']};
     	menus=data.items;

     	for(var i=0;i<data.items.length;i++){
     		$('tbody').append(createSingleRow(data.items[i],i));
     		console.log("Adding "+data.items[i].name);
     	}
 	}


    

     function saveMenu(){
     	console.log("Creating the menu item");
     	$('.loader_save_update').show();
     	var title=$('#title').val();
     	console.log("Add / Update category item id:"+editId+"  "+title);
     	


     	var topublish={
     		title:title,
     	}
     	console.log(topublish);
     	if(editId!=null){
            //We are doing update
            console.log("We are doing update to "+editId);
           updateClassObject({
            	objectClass:"menucategory",
            	fields:topublish,
            	id:editId,
            	photo:'image',
            	listener:function(e){
               window.location = 'menucategory.php';
           }
       });
            
        }else{
            
            	console.log("We are creating new menu item");
            	addClassObject({
            		objectClass:"menucategory",
            		fields:topublish,
            		photo:'image',
            		listener:function(e){
            			window.location = 'menucategory.php';
            		}
            	})
            
            
            
        }

    }

    function editMenu(index){
    	restartForm();
    	console.log("Edit index "+index);
    	$('#form_name').html('<?php __('update_category'); ?>');
    	$('#save_button').html('<?php __('update'); ?>');
    	var menu=menus[index];
    	editId=menu.id;
    	$('#title').val(menu.title);
    	if(menu.photo){
    		if( menu.photo.urls&& menu.photo.urls.medium_500){
    			$("#preview").attr('src', menu.photo.urls.medium_500);
    		}else{
    			alert('<?php __("image_notify"); ?>');
    		}

    	}

    	if(menu.prices){
    		for(var i=0;i<menu.prices.length;i++){
    			$('#price_holder').append(createAddonPrice(menu.prices[i].name,menu.prices[i].value));
    		}
    	}

         //Add prices
         
     }

     function startDeleteMenu(index){
     	console.log("Delete index "+index);

     	var menuId=menus[index].id;
     	deleteClassObject({
     		listener:function(e){fetchAndUpdateList(1);},
     		id:menuId,
     		objectClass:"menuorder"
     	})


     }

     function  deleteMenu(index) {


     		if (confirm("<?php __('sure_delete');?>") == false) {
     			return;
     		}

     		
     		startDeleteMenu(index); 

     	};


     

   
     jQuery(document).ready(function() {    
     	$('.loader_save_update').hide();
     	fetchAndUpdateList();


   



     	$('a.saveMenu').click(saveMenu);



     	$('#sample_2').on('click','a.edit', function (e) {
     		e.preventDefault();

     		/* Get the row as a parent of the link that was clicked on */
     		var nCell = $(this).parents('td')[0];
     		var aPos = $('#sample_2').dataTable().fnGetPosition( nCell );
     		editMenu(aPos[0]); 
     	});

     	

     });
</script>

	</body>
</html>