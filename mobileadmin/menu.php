<!DOCTYPE html>
<html>
<?php
   include 'static/vars.php';
   include_once("language/lang.php");
   $sm="2;1"; //Selected menu
?>
	<head>
	<title><?php __('menu'); ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="description" content="">
	<meta name="keywords" content="admin, bootstrap,admin template, bootstrap admin, simple, awesome">
	<meta name="author" content="">

	<!-- Bootstrap -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/third/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/style-responsive.css" rel="stylesheet">
	<link href="assets/css/animate.css" rel="stylesheet">
	<link href="assets/third/morris/morris.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/third/nifty-modal/css/component.css">
	<link rel="stylesheet" href="assets/third/sortable/sortable-theme-bootstrap.css"> 
	<link rel="stylesheet" href="assets/third/icheck/skins/minimal/grey.css"> 
	<link rel="stylesheet" href="assets/third/select/bootstrap-select.min.css"> 
	<link rel="stylesheet" href="assets/third/summernote/summernote.css" />
	<link rel="stylesheet" href="assets/third/magnific-popup/magnific-popup.css"> 
	<link rel="stylesheet" href="assets/third/pace/pace-theme-minimal.css">
	<link rel="stylesheet" href="assets/third/datepicker/css/datepicker.css"/>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="assets/img/favicon.ico">
	</head>
	<body class="tooltips">
	
	
	<!-- Begin page -->
	<div class="container">
		<!-- Modal Dialog -->
		<!-- Start sidebar menu -->
		<!-- Start right content -->
		<?php
		    include_once("static/modal.php");
   			include_once("static/navigation.php");
   			include_once("static/topbar.php");
		?>
		<!-- End of sidebar menu  and top bar-->
			
			
			
			<!-- ============================================================== -->
			<!-- Start Content here -->
			<!-- ============================================================== -->
            <div class="body content rows scroll-y">
				
				<!-- Page header -->
				<div class="page-heading animated fadeInDownBig">
					<h1><?php __('menu_items'); ?> <small><?php __('menu_items_desc'); ?></small></h1>
				</div>
				<!-- End page header -->
				
				
				<!-- List of products -->
				<div class="col-md-6">
				<div class="box-info full">
					<h2><?php __('menu_items_table_name'); ?></h2>
						
						<div class="data-table-toolbar">
							<div class="row">
								<div class="col-md-4">
									<img class="loader" src="assets/img/ajax-loader.gif" />
								</div>
								<div class="col-md-4">
									
								</div>
								<div class="col-md-4">
									<a href="repositionresmenu.php" class="btn btn-primary"><i class="fa fa-refresh"></i> <?php __('reposition'); ?></a>
								</div>
								
							</div>
						</div>
						
					<div class="table-responsive">
						<table data-sortable class="table table-hover table-striped">
							<thead>
								<tr>
									<th><?php __('menu_item_name'); ?></th>
									<th data-sortable="false"><?php __('options'); ?></th>
								</tr>
							</thead>
							
							<tbody>
							
							</tbody>
						</table>
					</div>
						
					<div class="data-table-toolbar">
						<ul class="pagination">
						 
						</ul>

					</div>
					<div></div>
				</div>
				</div>

				<!-- Start of the ADD Form -->
				<div class="col-sm-6">
						
						<div class="box-info animated fadeInDown">
						<h2 id="form_name"><?php __('add_new_item'); ?></h2>
							<div class="additional-btn">					  
								<a class="additional-icon" href="#" data-toggle="collapse" data-target="#horizontal-form"><i class="fa fa-chevron-down"></i></a>
							</div>
							
							<div id="horizontal-form" class="collapse in">
								<form id="add_post" class="form-horizontal" role="form">
									<?php 
										fieldGenerator(___('title'),"title",___('product_title'));
										fieldGenerator(___('intro'),"intro",___('short_text'));
										selectGenerator(___('category'),"category"); 
										
										textAreaGenerator(___('product_description'),"textArea",___('some_info_for_product'),4,12);
									 ?>

									<br /><hr/>
									<h5><?php __('prices');?></h5>
									<span>
											<div class="col-sm-8">
													<input name="product_price_name" id="product_price_name" value="" placeholder="<?php __('product_price_name');?>" class="inputtext input_middle required form-control" type="text">
												</div>
												<div class="col-sm-4">
													<a id="addprice" class="btn btn-green btn-default"><span><?php __('new_price');?></span></a>
												</div>

											

											</span><br ?>
											<span id="price_holder"></span>

									<br />
 									<hr />
 									<h5><?php __('photo');?></h5>
									<div class="form-group">
										<div class="col-sm-8">
											<img  id="preview" />
											<input  name="image" id="image"  type="file" class="btn btn-default" title="<?php __('select_photo');?>">
										</div>
									 </div>
									 <span class="label label-danger label-default">620x620px</span>
									 <hr/>
								
								  <div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
									  <a id="save_button"  class="btn btn-default saveMenu"><?php __('save');?></a><img  class="loader_save_update" src="assets/img/ajax-loader.gif" />
									</div>
								  </div>
								</form>
							</div>
						</div>
						
					</div>
				
				<!-- End of your awesome content -->
			
			
            </div>
			<!-- ============================================================== -->
			<!-- End content here -->
			<!-- ============================================================== -->	
        </div>
		<!-- End right content -->
		
		
		
		
		
		<!-- the overlay modal element -->
		<div class="md-overlay"></div>
		<!-- End of eoverlay modal -->
		
		
		
	</div>
	<!-- End of page -->

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="assets/js/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/third/knob/jquery.knob.js"></script>
		<script src="assets/third/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="assets/third/morris/morris.js"></script>
		<script src="assets/third/nifty-modal/js/classie.js"></script>
		<script src="assets/third/nifty-modal/js/modalEffects.js"></script>
		<script src="assets/third/sortable/sortable.min.js"></script>
		<script src="assets/third/select/bootstrap-select.min.js"></script>
		<script src="assets/third/summernote/summernote.js"></script>
		<script src="assets/third/magnific-popup/jquery.magnific-popup.min.js"></script> 
		<script src="assets/third/pace/pace.min.js"></script>
		<script src="assets/third/input/bootstrap.file-input.js"></script>
		<script src="assets/third/datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/third/icheck/icheck.min.js"></script>
		<script src="assets/third/wizard/jquery.easyWizard.js"></script>
		<script src="assets/third/wizard/scripts.js"></script>
		<script src="assets/js/lanceng.js"></script>

		<!-- Cloud connect scripts -->
		<script type="text/javascript" src="assets/scripts/cocoafish-1.2.js"></script> 
    	<script type="text/javascript" src="assets/scripts/utils.js"></script>    
    	<script type="text/javascript" src="assets/scripts/cloudconnect/objects.js"></script> 


    	<script>



 var editId=null;
 var menus=[];
 var categories=[];
 var currency="$";
 var per_page=5;

 function fetchAndUpdateList(page){
 	$('.loader').show(); //Show the loading indicator
 	retreiveObjectsFull({
 		objectClass:"menu",
 		query:{
 			per_page:per_page,
 			page:page,
 			order:"updated_at"
 		},
 		listener:function(data){
 			setUpMenu(data)
 		}
 	})
 };

 function fetchAndUpdateCategorList(){

 	retreiveObjects({
 		objectClass:"menucategory",
 		listener:function(data){
 			setUpCategories({
 				items:data,
 			})
 		}
 	})
 };

 function fetchAndUpdateSettings(){

 	retreiveObjects({
 		objectClass:"settings",
 		listener:function(data){
 			setUpSettings({
 				items:data,
 			})
 		}
 	})
 };

 function setUpSettings(data){

 	console.log(data.items.length);
 	if(data.items.length>0){
 		settings=data.items[0];

 		if(settings.config){
 			config=settings.config;
 			currency=config.currency;
 			$('#currency').html(currency);
 			console.log(currency);
 		}
 	}



 }

 function setUpCategories(data){
 	categories=data.items;
 	var appendcategory="";
 	for(var i=0;i<data.items.length;i++){
 		console.log("Adding category"+data.items[i].title);
 		appendcategory+='<option value="'+data.items[i].id+'">'+data.items[i].title+'</option>';
 	}
 	$('#category').html(appendcategory);
 }




 function restartForm(){
 	editId=null;
 	$('#price_holder').html("");
 	document.getElementById("add_post").reset();
         //CKEDITOR.instances.content.setData("<p></p>");
         $('#textArea').val("");
        // $("#preview").attr('src', "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image");
     }


     function createSingleRow(product,index){
     		var ret='<tr>';
									ret+='<td>'+product.title+'</td>';
									ret+='<td>';
										ret+='<div class="btn-group btn-group-xs">';
											ret+='<a onClick="deleteMenu(\''+index+'\');" class="btn btn-default" data-toggle="tooltip"  title="<?php __('delete'); ?>" ><i class="fa fa-power-off"></i></a>';
											ret+='<a onClick="editMenu(\''+index+'\');" data-toggle="tooltip" title="<?php __('edit'); ?>" class="btn btn-default"><i class="fa fa-edit"></i></a>';
										ret+='</div>';
									ret+='</td>';
								ret+='</tr>';
			return ret;
     }

     function createPagnitation(numPages,current){
     	var paging="";
     	for(var i=0; i<numPages;i++){
     		paging+='<li class="'+(current==(i+1)?"active":"")+'"><a href="#" onClick="fetchAndUpdateList('+(i+1)+')">'+(i+1)+'</a></li>';
     	}
     	return paging;
     }



     function setUpMenu(raw){
     	$('tbody').html(""); //Remove old rows
     	$('.loader').hide(); //Hide the loading indicator
     	$('.pagination').html(createPagnitation(raw.meta.total_pages,raw.meta.page)); //Clear pagnitation
     	//restartForm();
     	data={items:raw.response['menu']};
     	menus=data.items;
     	for(var i=0;i<data.items.length;i++){
     		$('tbody').append(createSingleRow(data.items[i],i));
     		console.log("Adding "+data.items[i].title);
     	}
     }

     function saveMenu(){
     	$('.loader_save_update').show();
     	console.log("Creating the menu item");
     	var title=$('#title').val();
     	var intro=$('#intro').val();
     	var category=$('#category').val();           
     	var content=$('#textArea').val();
     	console.log("Add / Update menu item id:"+editId+"  "+title+"   "+intro+"  "+content);
     	var serealized=$('form').serializeObject();
     	console.log("Serealized:"+JSON.stringify(serealized));
     	var obj=JSON.parse(JSON.stringify(serealized));
     	var prices=[];
     	for (var prop in obj) {
     		if(obj.hasOwnProperty(prop)&&prop.substring(0, 5) === 'price'){
     			console.log(prop.substring(5) + " = " + obj[prop]);
     			prices.push({
     				name:prop.substring(5),
     				value:obj[prop],
     			})
     		}
     	}


     	var topublish={
     		title:title,
     		category:category,
     		intro:intro,
     		content:content,
     		prices:prices,
     	}
     	console.log(topublish);
     	if(editId!=null){
            //We are doing update
            console.log("We are doing update to "+editId);
           updateClassObject({
            	objectClass:"menu",
            	fields:topublish,
            	id:editId,
            	photo:'image',
            	listener:function(e){
               window.location = 'menu.php';
           }
       });
            
        }else{
            //Update
            if(!topublish.category){
            	alert("Please add category first");
            }else{
            	console.log("We are creating new menu item");
            	addClassObject({
            		objectClass:"menu",
            		fields:topublish,
            		photo:'image',
            		listener:function(e){
            			window.location = 'menu.php';
            		}
            	})
            }
            
            
        }

    }

    function editMenu(index){
    	restartForm();
    	console.log("Edit index "+index);
    	$('#form_name').html('<?php __('update_item'); ?>');
    	$('#save_button').html('<?php __('update'); ?>');
    	var menu=menus[index];
    	editId=menu.id;
    	$('#title').val(menu.title);
    	$('#intro').val(menu.intro);
    	$('#category').val(menu.category);
    	$('#textArea').val(menu.content);
    	if(menu.photo){
    		if( menu.photo.urls&& menu.photo.urls.medium_500){
    			$("#preview").attr('src', menu.photo.urls.medium_500);
    		}else{
    			alert('<?php __("image_notify"); ?>');
    		}

    	}

    	if(menu.prices){
    		for(var i=0;i<menu.prices.length;i++){
    			$('#price_holder').append(createAddonPrice(menu.prices[i].name,menu.prices[i].value));
    		}
    	}

         //Add prices
         
     }

     function startDeleteMenu(index){
     	console.log("Delete index "+index);

     	var menuId=menus[index].id;
     	deleteClassObject({
     		listener:function(e){fetchAndUpdateList(1);},
     		id:menuId,
     		objectClass:"menu"
     	})


     }

     function  deleteMenu(index) {


     		if (confirm("<?php __('sure_delete');?>") == false) {
     			return;
     		}

     		
     		startDeleteMenu(index); 

     	};


     function makeid()
	{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
	}

   function removeId(id){
    $('#'+id).html("");
   }


     function createAddonPrice(priceName,priceValue){
        var randomID=makeid();
     	var addon='<div id="'+randomID+'" class="form-group"><hr />';
     	addon+='<label for="title" class="col-sm-2 control-label input-inline">'+priceName+':</label>';
     	addon+='<div class="col-sm-6">';
     	addon+='<input type="text"  value="'+priceValue+'" name="price'+priceName+'"  class="inputtext input_middle required form-control price">';
     	addon+='</div">';
     	addon+='<div class="col-sm-4">';
     	addon+='<a onClick="removeId(\''+randomID+'\')" ><?php __("remove"); ?></a>';
     	addon+='</div>';
     	addon+='</div>';
     	return addon;
     }



     jQuery(document).ready(function() {    
     	$('.loader_save_update').hide();
     	fetchAndUpdateList();
     	fetchAndUpdateCategorList();
     	//fetchAndUpdateSettings();


     	$.fn.serializeObject = function() {
     		var o = Object.create(null),
     		elementMapper = function(element) {
     			element.name = $.camelCase(element.name);
     			return element;
     		},
     		appendToResult = function(i, element) {
     			var node = o[element.name];

     			if ('undefined' != typeof node && node !== null) {
     				o[element.name] = node.push ? node.push(element.value) : [node, element.value];
     			} else {
     				o[element.name] = element.value;
     			}
     		};

     		$.each($.map(this.serializeArray(), elementMapper), appendToResult);
     		return o;
     	};

     	$("a#addprice").click(function() {
     		var priceName=$('input#product_price_name').val();
     		$('input#product_price_name').val("")
     		if(priceName==""){
     			$.gritter.add({
     				title: 'Error',
     				text: 'Price name is empty.',
     				class_name: 'gritter-light'
     			});
     			return null;
     		}
     		console.log("Add new price:"+priceName);
     		var addon=createAddonPrice(priceName,"0.00");
     		$('#price_holder').append(addon);
     	});



     	$('a.saveMenu').click(saveMenu);



     	$('#sample_2').on('click','a.edit', function (e) {
     		e.preventDefault();

     		/* Get the row as a parent of the link that was clicked on */
     		var nCell = $(this).parents('td')[0];
     		var aPos = $('#sample_2').dataTable().fnGetPosition( nCell );
     		editMenu(aPos[0]); 
     	});

     	

     });
</script>

	</body>
</html>