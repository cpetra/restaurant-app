<?php

echo '<div class="right content-page">
		<div class="header content rows-content-header">
			
			<!-- Button mobile view to collapse sidebar menu -->
			<button class="button-menu-mobile show-sidebar">
				<i class="fa fa-bars"></i>
			</button>
			
             <div class="navbar navbar-default" role="navigation">
				  <div class="container">
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<i class="fa fa-angle-double-down"></i>
					  </button>
					</div>
					<div class="navbar-collapse collapse">
					
					  <ul class="nav navbar-nav">
						<li><a><i class="fa fa-flag"></i></a></li>
						<li class="dropdown">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$langName.'  <i class="fa fa-chevron-down i-xs"></i></a>
						  <ul class="dropdown-menu animated half flipInX">
							<li><a href="?lang=en&langName=English">English  </a></li>
							<li><a href="?lang=es&langName=Spanish">Spanish  </a></li>
							<li><a href="?lang=it&langName=Italian">Italian  </a></li>
						  </ul>
						</li>
					  </ul>

					  
					  

					  
					  
					</div><!--/.nav-collapse -->
				  </div>
				</div>
            </div>';
?>