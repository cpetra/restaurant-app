//##################### DATA ##################
var VARS =require('common/globals'); //Here we store all global variables / settings
if(VARS.displayAdMobAdsNavigation){var ADS=require('common/ads');} //AD LIBRARY

function ApplicationMetro(sections)
{
	//create module instance
	var tgMetroHolder = Ti.UI.createTabGroup();
	
	var self = Ti.UI.createWindow({
	    title:Ti.App.getName(),
		backgroundColor : VARS.windowBackgroundColor,
		navBarHidden : true,
		tabBarHidden : true,
		barColor : VARS.barColor,
		orientationModes : [Titanium.UI.PORTRAIT]
	})
	
	var tabMain = Ti.UI.createTab({
		window: self
	});
	
	//AD
	MetroHeight="100%";
	if(VARS.displayAdMobAdsNavigation)
	{
		var theAd=ADS.requestAd({win:self});
		MetroHeight=VARS._dpiHeight-VARS.AdHeight+45; //Because in metro there is no navigation
        if(VARS._platform==VARS._android){
            MetroHeight=(VARS._dpiHeight-VARS.androidAdHeight)+"dp";
        }
	}
	
	//Main Scroller
	var metroScroll=Ti.UI.createScrollView({
		top:0,
		width:"100%",
		height:MetroHeight,
		contentHeight:"auto",
		contentWidth:"100%",
		backgroundColor:VARS.separatoColor,
	})
	
	//Data for knowing where we should put the elements
	var x=1;
	var y=0;
	var wHeight = Ti.Platform.displayCaps.platformHeight;
	var	wWidth = Ti.Platform.displayCaps.platformWidth;
	var dashW=(VARS._dpiWidth/2)-2;
	//Generate metro style dash items
	for(var i=0;i<sections.length;i++)
	{	
		var singleDash=Ti.UI.createView({
			top:y,
			left:x+"dp",
			width:dashW+"dp",
			height:wHeight/3-1,
			backgroundColor:VARS.rowColor,
			win:sections[i].win,
		});
		sections[i].win.containingTab=tabMain;
		
		//Create Label
		var heightIndex=2.8;
		var fontSize=VARS.h3;
		if(VARS._platform==VARS._android){fontSize=VARS.h1;heightIndex=2.6}
		if(VARS._platform==VARS._iPad){heightIndex=2.3}
		var dashName=Ti.UI.createLabel({
			text:sections[i].title,
			left:"0dp",
			textAlign:"center",
			top:(wHeight/12)*heightIndex,
			font:VARS.h3,
			width:dashW+"dp",
			color:VARS.textColor,
			touchEnabled:false,
		})
		singleDash.add(dashName);
		
		//The icon
		imageHeight=null;
		if(VARS._platform==VARS._android){imageHeight="64dp"}
		var metroIcon=Ti.UI.createImageView({
			//left:"15dp",
			touchEnabled:false,
			top:(wHeight/12),
			height:imageHeight,
			image:"/images/metro/"+sections[i].icon,
		})
		singleDash.add(metroIcon);
		
		singleDash.addEventListener('click',function(e)
		{
			tabMain.open(e.source.win);
		})	
		
		metroScroll.add(singleDash);
		
		//Recalculate new x,y
		if(((i+1)%2)==0)
		{
			x=1;
			y+=wHeight/3+1;
		}
		else
		{
			x+=dashW+2;
		}
	}
	
	self.add(metroScroll);
	self.containingTab=tabMain;
	tgMetroHolder.addTab(tabMain);
	if(VARS._platform==VARS._android){return self;}
	else {return tgMetroHolder;}
	
	
}
module.exports = ApplicationMetro;
