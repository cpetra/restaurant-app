exports.separatoColor="black";
exports.rowColor="#1f1f1f";
exports.rowOddColor="#2a2a2a";
exports.windowBackgroundColor="#2a2a2a";
exports.barColor="#3e3e3c";

exports.textColor="#000000";
exports.textSecondColor="#3e3e3c";
exports.listColors=["#fe4819","#d1005d","#0081ab","#009a3d"];

exports.fontColor="#ffffff";
exports.imageFrameColor="white";
exports.formTextColor="black";
exports.formBgColor="white";
exports.buttonColor="#010101";