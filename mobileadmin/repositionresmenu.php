<!DOCTYPE html>
<html>
<?php
include 'static/vars.php';
include_once("language/lang.php");
   //TODO
   $sm="2;1"; //Selected menu
   ?>
   <head>
   	<title><?php __('repositon_restaurant_menu_items'); ?></title>
   	<meta charset="utf-8">
   	<meta name="viewport" content="width=device-width, initial-scale=1.0">
   	<meta name="apple-mobile-web-app-capable" content="yes" />
   	<meta name="description" content="">
   	<meta name="keywords" content="admin, bootstrap,admin template, bootstrap admin, simple, awesome">
   	<meta name="author" content="">

   	<!-- Bootstrap -->
   	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
   	<link href="assets/third/font-awesome/css/font-awesome.min.css" rel="stylesheet">
   	<link href="assets/css/style.css" rel="stylesheet">
   	<link href="assets/css/style-responsive.css" rel="stylesheet">
   	<link href="assets/css/animate.css" rel="stylesheet">
   	<link href="assets/third/morris/morris.css" rel="stylesheet">
   	<link rel="stylesheet" href="assets/third/nifty-modal/css/component.css">
   	<link rel="stylesheet" href="assets/third/sortable/sortable-theme-bootstrap.css"> 
   	<link rel="stylesheet" href="assets/third/icheck/skins/minimal/grey.css"> 
   	<link rel="stylesheet" href="assets/third/select/bootstrap-select.min.css"> 
   	<link rel="stylesheet" href="assets/third/summernote/summernote.css" />
   	<link rel="stylesheet" href="assets/third/magnific-popup/magnific-popup.css"> 
   	<link rel="stylesheet" href="assets/third/pace/pace-theme-minimal.css">
   	<link rel="stylesheet" href="assets/third/datepicker/css/datepicker.css"/>
   	<link rel="stylesheet" href="assets/css/nestable.css"/>


   	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="assets/img/favicon.ico">
</head>
<body class="tooltips">
	
	
	<!-- Begin page -->
	<div class="container">
		<!-- Modal Dialog -->
		<!-- Start sidebar menu -->
		<!-- Start right content -->
		<?php
		include_once("static/modal.php");
		include_once("static/navigation.php");
		include_once("static/topbar.php");
		?>
		<!-- End of sidebar menu  and top bar-->



		<!-- ============================================================== -->
		<!-- Start Content here -->
		<!-- ============================================================== -->
		<div class="body content rows scroll-y">

			<!-- Page header -->
			<div class="page-heading animated fadeInDownBig">
				<?php __('reposition_restaurant_menu_page_title'); ?>
			</div>
			<!-- End page header -->


			<!-- Your awesome content goes here -->
			<div class="row">
				<div class="col-sm-6">
					<div class="box-info">
						<h2><?php __('reposition_categories'); ?></h2>
						<div class="dd" id="nestable1">
						</div>
            
					</div>
          <a id="save_button"  class="btn btn-default saveMenuCategory"><?php __('save_category_order');?></a><img  class="loader_save_update" src="assets/img/ajax-loader.gif" />
				</div>
				<div class="col-sm-6">
					<div class="box-info">
						<h2><?php __('reposition_menus_from_categories'); ?></h2>
            <?php
              selectGenerator(___('category'),"category"); 
            ?>
            <hr /><br />
						<div class="dd" id="nestable2">
							<ol class="dd-list" style="padding:10px;">
								<li class="dd-item" ></li>
							</ol>
						</div>
					</div>
          <a id="save_button_menu_items"  class="btn btn-default saveMenu"><?php __('save_menu_order');?></a><img  class="loader_save_update_menu_items" src="assets/img/ajax-loader.gif" />
				</div>
			</div>

			








			<textarea style="display:none;"  id="nestable1-output"></textarea>
			<textarea style="display:none;" id="nestable2-output"></textarea>

			<!-- End of your awesome content -->
			
			
		</div>
		<!-- ============================================================== -->
		<!-- End content here -->
		<!-- ============================================================== -->	
	</div>
	<!-- End right content -->





	<!-- the overlay modal element -->
	<div class="md-overlay"></div>
	<!-- End of eoverlay modal -->



</div>
<!-- End of page -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/third/knob/jquery.knob.js"></script>
<script src="assets/third/slimscroll/jquery.slimscroll.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="assets/third/morris/morris.js"></script>
<script src="assets/third/nifty-modal/js/classie.js"></script>
<script src="assets/third/nifty-modal/js/modalEffects.js"></script>
<script src="assets/third/sortable/sortable.min.js"></script>
<script src="assets/third/select/bootstrap-select.min.js"></script>
<script src="assets/third/summernote/summernote.js"></script>
<script src="assets/third/magnific-popup/jquery.magnific-popup.min.js"></script> 
<script src="assets/third/pace/pace.min.js"></script>
<script src="assets/third/input/bootstrap.file-input.js"></script>
<script src="assets/third/datepicker/js/bootstrap-datepicker.js"></script>
<script src="assets/third/icheck/icheck.min.js"></script>
<script src="assets/third/wizard/jquery.easyWizard.js"></script>
<script src="assets/third/wizard/scripts.js"></script>
<script src="assets/js/lanceng.js"></script>
<script src="assets/third/nestable/jquery.nestable.js"></script>

<!-- Cloud connect scripts -->
<script type="text/javascript" src="assets/scripts/cocoafish-1.2.js"></script> 
<script type="text/javascript" src="assets/scripts/utils.js"></script> 
<script type="text/javascript" src="assets/scripts/cloudconnect/objects.js"></script>
<script type="text/javascript" src="config/restaurant.json"></script>

<script>

 var settings=null;
 var firstCategoryId="<?php if(isset($_GET['catid'])){echo $_GET['catid'];}else{echo '0';}; ?>";
 console.log(firstCategoryId);
 var categoriesAssociativeId=[];
 var menuByCategory=[];
 var menuAssociativeId=[];
  function fetchAndUpdateSettings(){
  	$('.loader_save_update').show();
    $('.loader_save_update_menu_items').show();
   retreiveObjects({
    objectClass:"settings",
    query:{response_json_depth:1,page:1,per_page:50},
    listener:function(data){
      fetchAndUpdateMenuList(firstCategoryId);
      setUpSettings({
        items:data,
      })
    }
  })
 };

 function fetchAndUpdateCategorList(){

  retreiveObjects({
    query:{per_page:500},
    objectClass:"menucategory",
    listener:function(data){
      fetchAndUpdateSettings();
      setUpCategories({
        items:data,
      })
    }
  })
 };

 function fetchAndUpdateMenuList(id){

  retreiveObjects({
    query:{per_page:500,where:JSON.stringify({category:id})},
    objectClass:"menu",
    listener:function(data){
      if(settings.resmenupositions){
        navigation=settings.resmenupositions;
      }else {
        navigation=null;
      }
      console.log("Menus in category");
     menuByCategory=data;
     //Make it associative
     for(var i=0;i<menuByCategory.length;i++){
      menuAssociativeId[menuByCategory[i].id]=menuByCategory[i];
      menuAssociativeId[menuByCategory[i].id].used=false;
     }

     console.log(menuByCategory);
      createTheMenuList(navigation);
    

     
   }
 })
};

 function setUpCategories(data){
  categories=data.items;
  var appendcategory="";
  for(var i=0;i<data.items.length;i++){
    console.log("Adding category"+data.items[i].title);
    appendcategory+='<option value="'+data.items[i].id+'">'+data.items[i].title+'</option>';
    categoriesAssociativeId[data.items[i].id]=data.items[i];
  }
  if(firstCategoryId=="0"){
    firstCategoryId=data.items.length>0?data.items[0].id:0;
  }
  $('#category').html(appendcategory);
  if(firstCategoryId!="0"){
     $('#category').val(firstCategoryId);
  }
 }

 function saveSettingsMenu(){
  $('.loader_save_update_menu_items').show();
  if(settings!=null){
    //We can save
    var fields={};
    fields[firstCategoryId]=$('#nestable2').nestable('serialize');
    

    if(settings.id){
      //We have settings before
      console.log('Modify existing settings');
      //We maybe have some some other ordering there
      if(settings.resmenupositions){
        console.log("We have ordering for other categories");
        settings.resmenupositions[firstCategoryId]=$('#nestable2').nestable('serialize');
      }else{
        console.log("We have settings, but this is the first time we are creating a restaurant menu positinsing");
        settings.resmenupositions=fields;
      }
      
      updateClassObject({
        objectClass:"settings",
        fields:settings,
        id:settings.id,
        photo:null,
        listener:function(e){
          $('.loader_save_update_menu_items').hide();
          window.location = 'repositionresmenu.php?cid='+firstCategoryId;
        }
      });
    }else{
      console.log('Create new settings');
      topublish={
        resmenupositions:fields
      }
      console.log(topublish);

      addClassObject({
        objectClass:"settings", 
        fields:topublish,
        photo:null,
        listener:function(e){
          window.location = 'repositionresmenu.php?cid='+firstCategoryId;
        }
      });

    }
  }else{
      alert("Please wait until the settings are fetch!")
    }
  }



 function saveSettingsCategory(){
 	$('.loader_save_update').show();
  if(settings!=null){
    //We can save
    var fields={
      category:$('#nestable1').nestable('serialize'),
    }
    

    if(settings.id){
      //We have settings before
      console.log('Modify existing settings');
      settings.categorypositions=fields;
      updateClassObject({
        objectClass:"settings",
        fields:settings,
        id:settings.id,
        photo:null,
        listener:function(e){
        	$('.loader_save_update').hide();
        }
      });
    }else{
      console.log('Create new settings');
      topublish={
        categorypositions:fields
      }
      console.log(topublish);

      addClassObject({
        objectClass:"settings",
        fields:topublish,
        photo:null,
        listener:function(e){
          window.location = 'repositionresmenu.php?cid='+firstCategoryId;
        }
      });

    }
  }else{
      alert("Please wait until the settings are fetch!")
    }
  }

  function createMenuListItem(id){
  if(menuAssociativeId[id]){ 
    listRow='<ol class="dd-list">';
                listRow='<li class="dd-item" data-id="'+id+'">';
                  listRow+='<div class="dd-handle">'+menuAssociativeId[id].title+'</div>';
                listRow+='</li>';
                menuAssociativeId[id].used=true;
    return listRow;
    }
    else {
    return "";
    }
  }


  function createListItem(id){
        if(categoriesAssociativeId[id]){
  	listRow='<ol class="dd-list">';
								listRow='<li class="dd-item" data-id="'+id+'">';
									listRow+='<div class="dd-handle">'+categoriesAssociativeId[id].title+'</div>';
								listRow+='</li>';
                categoriesAssociativeId[id].used=true;
  	return listRow;
  	}else{
  	return "";
  	}
  }

  function createTheMenuList(nav){
    if(nav&&nav[firstCategoryId]){
      console.log("Display the menus for category "+firstCategoryId+" based on previous ordering" );
      var listHandler='<ol class="dd-list">';
      for(var i=0;i<nav[firstCategoryId].length;i++){
        listHandler+=createMenuListItem(nav[firstCategoryId][i].id);
      }

      //There is case when, user has added a new category, and this is not in the list
      for (key in menuAssociativeId ){
    if (menuAssociativeId.hasOwnProperty(key)&&!menuAssociativeId[key].used) {
        console.log(menuAssociativeId[key].title);
        listHandler+=createMenuListItem(menuAssociativeId[key].id);
    }
}


      listHandler+="</ol>";
      $('#nestable2').html(listHandler);

    }else{
      console.log("Display the menus for category "+firstCategoryId+" based on NEW ordering" )


      var listHandler='<ol class="dd-list">';
      for(var i=0;i<menuByCategory.length;i++){
        listHandler+=createMenuListItem(menuByCategory[i].id);
      }
      listHandler+="</ol>";
      $('#nestable2').html(listHandler);
    }

   
     // activate Nestable for list 2
    $('#nestable2').nestable({
      group: 2,
      maxDepth:1,
    })
    .on('change', updateOutput);

    updateOutput($('#nestable2').data('output', $('#nestable2-output')));

  }

  function createTheList(nav){
  	if(nav&&nav.category){
      //We have saved previous ordering
      console.log("There is  previous ordering");
  		var listHandler='<ol class="dd-list">';
  		for(var i=0;i<nav.category.length;i++){
  			listHandler+=createListItem(nav.category[i].id);
  		}

      //There is case when, user has added a new category, and this is not in the list
      for (key in categoriesAssociativeId ){
    if (categoriesAssociativeId.hasOwnProperty(key)&&!categoriesAssociativeId[key].used) {
        console.log(categoriesAssociativeId[key].title);
        listHandler+=createListItem(categoriesAssociativeId[key].id);
    }
}

  		listHandler+="</ol>";
  		$('#nestable1').html(listHandler);
  	}else{
      //We haven't saved previous odering
      console.log("There is no previous ordering of the categories ordersing, display all as they appear");
      var listHandler='<ol class="dd-list">';
      for(var i=0;i<categories.length;i++){
        listHandler+=createListItem(categories[i].id);
      }
      listHandler+="</ol>";
      $('#nestable1').html(listHandler);
    }

  	
  		



  	 // activate Nestable for list 1
    $('#nestable1').nestable({
    	group: 1,
    	maxDepth:1,
    })
    .on('change', updateOutput);

   

    
    
    updateOutput($('#nestable1').data('output', $('#nestable1-output')));
  }



function setUpSettings(data){
 $('.loader_save_update').hide();
 $('.loader_save_update_menu_items').hide();
 console.log(data.items.length);
 if(data.items.length>0){
  settings=data.items[0];

  if(settings.categorypositions){
    navigation=settings.categorypositions;
  }else {
  	navigation=null;
  }
  
}else{
  settings={};
  navigation=null;
}

console.log(navigation);
createTheList(navigation);
};

var updateOutput = function(e)
	{
		var list   = e.length ? e : $(e.target),
		output = list.data('output');
		if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
		} else {
			output.val('JSON browser support required for this demo.');
		}
	};



jQuery(document).ready(function() {
fetchAndUpdateCategorList(); 
	//fetchAndUpdateSettings();
 	$('a.saveMenuCategory').click(saveSettingsCategory);

$('a.saveMenu').click(saveSettingsMenu);
  
    
    $( "#category" ).change(function(e) {
     window.location = 'repositionresmenu.php?catid='+this.value;
    });
   


});
</script>   
</body>
</html>