<!DOCTYPE html>
<html>
<?php
   include 'static/vars.php';
   include_once("language/lang.php");
   //TODO
   $sm="2;3"; //Selected menu
?>
	<head>
	<title><?php __('menu_pay'); ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="description" content="">
	<meta name="keywords" content="admin, bootstrap,admin template, bootstrap admin, simple, awesome">
	<meta name="author" content="">

	<!-- Bootstrap -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/third/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/style-responsive.css" rel="stylesheet">
	<link href="assets/css/animate.css" rel="stylesheet">
	<link href="assets/third/morris/morris.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/third/nifty-modal/css/component.css">
	<link rel="stylesheet" href="assets/third/sortable/sortable-theme-bootstrap.css"> 
	<link rel="stylesheet" href="assets/third/icheck/skins/minimal/grey.css"> 
	<link rel="stylesheet" href="assets/third/select/bootstrap-select.min.css"> 
	<link rel="stylesheet" href="assets/third/summernote/summernote.css" />
	<link rel="stylesheet" href="assets/third/magnific-popup/magnific-popup.css"> 
	<link rel="stylesheet" href="assets/third/pace/pace-theme-minimal.css">
	<link rel="stylesheet" href="assets/third/datepicker/css/datepicker.css"/>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="assets/img/favicon.ico">
	</head>
	<body class="tooltips">
	
	
	<!-- Begin page -->
	<div class="container">
		<!-- Modal Dialog -->
		<!-- Start sidebar menu -->
		<!-- Start right content -->
		<?php
		    include_once("static/modal.php");
   			include_once("static/navigation.php");
   			include_once("static/topbar.php");
		?>
		<!-- End of sidebar menu  and top bar-->
			
			
			
			<!-- ============================================================== -->
			<!-- Start Content here -->
			<!-- ============================================================== -->
            <div class="body content rows scroll-y">
				
				<!-- Page header -->
				<div class="page-heading animated fadeInDownBig">
					<h1><?php __('menupayment_page_title'); ?></small></h1>
				</div>
				<!-- End page header -->
				
				
				<!-- Your awesome content goes here -->
				
				
				<div class="box-info animated fadeInDown">
				<h2><?php __('menupayment_form_title'); ?></h2>
					<form class="form-horizontal" role="form">
						
					  <?php 

						$paymentOptions = array('1' =>___('payment_required'),'2' =>___('payment_optional'),'3' =>___('no_payment'),'4' =>___('dont_accept_orders') );
					  	selectGenerator(___('menupayment_label_payment'),"payment",$paymentOptions);
					  	fieldGenerator(___('menupayment_label_tax'),"tax",___('menupayment_hint_tax'));
					  	echo '<p class="help-block">'.___("menu_tax_help").'</p><br/>';
					  	fieldGenerator(___('menupayment_label_orderemail'),"orderemail",___('menupayment_hint_orderemail'));
					  	
					  	fieldGenerator(___('user_name_orders'),"orderuser",___('user_name_orders_hint'));
					  	fieldGenerator(___('password_orders'),"orderpass",___('password_orders_hint'));
						
						/*echo '<br /><h4>'.___('location').'</h4><hr />';
						fieldGenerator(___('menu_locations_label'),"menu_locations",___('menu_locations_hint'));
						echo '<p class="help-block">'.___("menu_locations_help").'</p><br/>';*/
					  	
					  	echo '<br /><h4>PayPal</h4><hr />';
					  	$paypalOptions = array('1' => ___('sandbox'),'2'=>___('live') );
					  	selectGenerator(___('menupayment_label_paypaltype'),"paypaltype",$paypalOptions);
					  	fieldGenerator(___('menupayment_label_paypalemail'),"paypalemail",___('menupayment_hint_paypalemail'));
					  	fieldGenerator(___('menupayment_label_paypalappid'),"paypalappid",___('menupayment_hint_paypalappid'));
					  	echo '<p class="help-block">'.___("menupayment_help_appid").'</p><br/>';
					  	fieldGenerator(___('menupayment_label_currency'),"currency",___('menupayment_hint_currency'));
					  	echo '<p class="help-block">'.___("menupayment_help_currecny").'</p><br/>';
					  	fieldGenerator(___('menupayment_label_ipnurl'),"ipnurl",___('menupayment_hint_ipnurl'));	
					  	echo '<p class="help-block">'.___("menupayment_help_ipnurl").'</p><br/>';			  	
					   ?>
					

					    <div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
									  <a id="save_button"  class="btn btn-default saveMenu"><?php __('save');?></a><img  class="loader_save_update" src="assets/img/ajax-loader.gif" />
									</div>
								  </div>

					</form>
				</div>
				
				
				<!-- End of your awesome content -->
			
			
            </div>
			<!-- ============================================================== -->
			<!-- End content here -->
			<!-- ============================================================== -->	
        </div>
		<!-- End right content -->
		
		
		
		
		
		<!-- the overlay modal element -->
		<div class="md-overlay"></div>
		<!-- End of eoverlay modal -->
		
		
		
	</div>
	<!-- End of page -->

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="assets/js/jquery.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/third/knob/jquery.knob.js"></script>
		<script src="assets/third/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="assets/third/morris/morris.js"></script>
		<script src="assets/third/nifty-modal/js/classie.js"></script>
		<script src="assets/third/nifty-modal/js/modalEffects.js"></script>
		<script src="assets/third/sortable/sortable.min.js"></script>
		<script src="assets/third/select/bootstrap-select.min.js"></script>
		<script src="assets/third/summernote/summernote.js"></script>
		<script src="assets/third/magnific-popup/jquery.magnific-popup.min.js"></script> 
		<script src="assets/third/pace/pace.min.js"></script>
		<script src="assets/third/input/bootstrap.file-input.js"></script>
		<script src="assets/third/datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/third/icheck/icheck.min.js"></script>
		<script src="assets/third/wizard/jquery.easyWizard.js"></script>
		<script src="assets/third/wizard/scripts.js"></script>
		<script src="assets/js/lanceng.js"></script>

		<!-- Cloud connect scripts -->
		<script type="text/javascript" src="assets/scripts/cocoafish-1.2.js"></script> 
    	<script type="text/javascript" src="assets/scripts/utils.js"></script> 
    	<script type="text/javascript" src="assets/scripts/cloudconnect/objects.js"></script>  

    	 <script>

  var settings=null;
  function fetchAndUpdateSettings(){
  	$('.loader_save_update').show();
   retreiveObjects({
    objectClass:"settings",
    query:{response_json_depth:1,page:1,per_page:50},
    listener:function(data){
      setUpSettings({
        items:data,
      })
    }
  })
 };

 function saveSettings(){
 	$('.loader_save_update').show();
  if(settings!=null){
    //We can save
    var fields={
      payment:$('#payment').val(),
      tax:$('#tax').val(),
      orderemail:$('#orderemail').val(),
      orderuser:$('#orderuser').val(),
      orderpass:$('#orderpass').val(),
      paypaltype:$('#paypaltype').val(),
      paypalemail:$('#paypalemail').val(),
      paypalappid:$('#paypalappid').val(),
      currency:$('#currency').val(),
      ipnurl:$('#ipnurl').val(),
      //menu_locations:$('menu_locations').val(),
    }
    

    if(settings.id){
      //We have settings before
      console.log('Modify existing settings');
      settings.payment=fields;
      updateClassObject({
        objectClass:"settings",
        fields:settings,
        id:settings.id,
        photo:null,
        listener:function(e){
        	window.location = 'menupayment.php';
        }
      });
    }else{
      console.log('Create new settings');
      topublish={
        payment:fields
      }
      console.log(topublish);

      addClassObject({
        objectClass:"settings",
        fields:topublish,
        photo:null,
        listener:function(e){
          window.location = 'menupayment.php';
        }
      });

    }
  }else{
      alert("Please wait until the settings are fetch!")
    }
  }

function setUpSettings(data){
	$('.loader_save_update').hide();
 console.log(data.items.length);
 if(data.items.length>0){
  settings=data.items[0];

  if(settings.payment){   
      
    paymentData=settings.payment;
    $('#payment').val(paymentData.payment);
    $('#tax').val(paymentData.tax);
    $('#orderemail').val(paymentData.orderemail);
    $('#orderpass').val(paymentData.orderpass);
    $('#orderuser').val(paymentData.orderuser);
    $('#paypaltype').val(paymentData.paypaltype);
    $('#paypalemail').val(paymentData.paypalemail);
    $('#paypalappid').val(paymentData.paypalappid);
    $('#currency').val(paymentData.currency);
    $('#ipnurl').val(paymentData.ipnurl);
   // $('#menu_locations').val(paymentData.ipnurl);
    
    


  }
  
}else{
  settings={};
}
};


jQuery(document).ready(function() {    
  fetchAndUpdateSettings();
  $('a.saveMenu').click(saveSettings);
});
</script>

	</body>
</html>