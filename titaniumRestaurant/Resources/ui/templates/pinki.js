exports.separatoColor="black";
exports.rowColor="#1f1f1f";
exports.rowOddColor="#2a2a2a";
exports.windowBackgroundColor="#2a2a2a";
exports.barColor="#d32b9d";
exports.fontColor="#ffffff";
exports.imageFrameColor="white";
exports.formTextColor="#d32b9d";
exports.formBgColor="white";
exports.buttonColor="#d32b9d";
exports.textColor="white";
exports.textSecondColor="#d32b9d";
exports.listColors=["#fe4819","#d1005d","#0081ab","#009a3d"];

//Optional
exports.videosCoversColor="#F2F2F2";
exports.videosWindowBackgroundColor="#E7E7E7";
exports.videosTextColor="#6D6D6D";

exports.buttonColor="#cd5a8d";
exports.buttonTopColor="#cd5a8d";
exports.buttonBottomColor="#b30352";

