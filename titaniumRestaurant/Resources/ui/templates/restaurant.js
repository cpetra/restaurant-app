exports.separatoColor="#e9e8d9";
exports.rowColor="#FFFFFF";
exports.rowOddColor="#e7b0b0";
exports.windowBackgroundColor="#400000";
exports.barColor="#400000";

exports.textColor="#3a3a3a";
exports.textSecondColor="#FFFFFF";
exports.listColors=["#fe4819","#d1005d","#0081ab","#009a3d"];

exports.fontColor="#ffffff";
exports.imageFrameColor="white";
exports.formTextColor="white";
exports.formBgColor="#8c171d";
exports.buttonColor="#8c021c";