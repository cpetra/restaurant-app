<?php
 

session_start();


function setSessionVariable($name,&$var,$musthave=FALSE,$default=""){
//Set app name
if(isset($_SESSION[$name]))
{
	//But it we have some data, change the value
	if(isset($_GET[$name])){
		$_SESSION[$name]=$_GET[$name];
	}
	else if(isset($_POST[$name])){
		$_SESSION[$name]=$_GET[$name];
	}

	//Already set up
	$var=$_SESSION[$name];	
}
else
{
	//It is nor set up
	if(isset($_GET[$name])){
		$_SESSION[$name]=$_GET[$name];
		//Already set up
		$var=$_SESSION[$name];
	}
	else if(isset($_POST[$name])){
		$_SESSION[$name]=$_GET[$name];
		//Already set up
		$var=$_SESSION[$name];
	} else if($musthave){
		//We don't have any value, but we must put something in session
		$_SESSION[$name]=$default;
		//Already set up
		$var=$_SESSION[$name];
	}




}

}



//Set language
$lang="en";
setSessionVariable("lang",$lang,TRUE,"en");

//Set language
$langName="en";
setSessionVariable("langName",$langName,TRUE,"English  ");

//Set adminName
$adminName="en";
setSessionVariable("adminName",$adminName,TRUE,"Admin");

//Set adminPhoto
$adminPhoto="";
setSessionVariable("adminPhoto",$adminPhoto,TRUE,"http://placehold.it/100&text=No%20Photo");

 function fieldGenerator($label,$id,$hint,$lblWidth=2,$fieldWidth=10){
   	echo '<div class="form-group">
						<label for="input-text" class="col-sm-'.$lblWidth.' control-label">'.$label.'</label>
						<div class="col-sm-'.$fieldWidth.'">
						  <input id="'.$id.'" name="'.$id.'" type="text" class="form-control" id="input-text" placeholder="'.$hint.'">
						</div>
					  </div>';
   }

   function colorFieldGenerator($label,$id,$color="#000000",$lblWidth=2,$fieldWidth=10){
   	echo '<div class="form-group">
			<label for="input-text" class="col-sm-'.$lblWidth.' control-label">'.$label.'</label>
			<div class="input-append color" id="'.$id.'Div"  data-color="'.$color.'">
				<div class="row">
					<div class="col-xs-6">
						<input class="span1 form-control" id="'.$id.'" type="text" value="'.$color.'">
					</div>
					<div class="col-xs-2">
						<span class="add-on">
      						<i style="width:30px; height:30px; background-color:"'.$color.'"></i>
      					</span>
					</div>
				</div>
			</div>
		</div>';
   }


function textAreaGenerator($label,$id,$hint,$lblWidth=2,$fieldWidth=10){
   	echo '<div class="form-group">
						<label for="input-text" class="col-sm-'.$lblWidth.' control-label">'.$label.'</label>
						<div class="col-sm-'.$fieldWidth.'">
						  <textarea id="'.$id.'" name="'.$id.'" placeholder="'.$hint.'" cols="25" rows="5" class="ckeditor textarea form-control  textarea_middle required"></textarea>
						</div>
					  </div>';
   }



 function selectGenerator($label,$id,$data=null,$lblWidth=2,$fieldWidth=10){
 	$options='';
 	if(isset($data)){

 		foreach ($data as $k => $v) {
 			$options.='<option value="'.$k .'">'.$v.'</option>';
		}
 	}

   	echo '<div class="form-group">
						<label for="input-text" class="col-sm-'.$lblWidth.' control-label">'.$label.'</label>
						<div class="col-sm-'.$fieldWidth.'">
						<select id="'.$id.'" name="'.$id.'" class="form-control">
							  '.$options.'
							</select>
						</div>
					  </div>';
   }

							




?>