<!DOCTYPE html>
<html>
<?php
include 'static/vars.php';
include_once("language/lang.php");
   //TODO
   $sm="8;2"; //Selected menu
   ?>
   <head>
   	<title><?php __('reposition_navigation'); ?></title>
   	<meta charset="utf-8">
   	<meta name="viewport" content="width=device-width, initial-scale=1.0">
   	<meta name="apple-mobile-web-app-capable" content="yes" />
   	<meta name="description" content="">
   	<meta name="keywords" content="admin, bootstrap,admin template, bootstrap admin, simple, awesome">
   	<meta name="author" content="">

   	<!-- Bootstrap -->
   	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
   	<link href="assets/third/font-awesome/css/font-awesome.min.css" rel="stylesheet">
   	<link href="assets/css/style.css" rel="stylesheet">
   	<link href="assets/css/style-responsive.css" rel="stylesheet">
   	<link href="assets/css/animate.css" rel="stylesheet">
   	<link href="assets/third/morris/morris.css" rel="stylesheet">
   	<link rel="stylesheet" href="assets/third/nifty-modal/css/component.css">
   	<link rel="stylesheet" href="assets/third/sortable/sortable-theme-bootstrap.css"> 
   	<link rel="stylesheet" href="assets/third/icheck/skins/minimal/grey.css"> 
   	<link rel="stylesheet" href="assets/third/select/bootstrap-select.min.css"> 
   	<link rel="stylesheet" href="assets/third/summernote/summernote.css" />
   	<link rel="stylesheet" href="assets/third/magnific-popup/magnific-popup.css"> 
   	<link rel="stylesheet" href="assets/third/pace/pace-theme-minimal.css">
   	<link rel="stylesheet" href="assets/third/datepicker/css/datepicker.css"/>
   	<link rel="stylesheet" href="assets/css/nestable.css"/>


   	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="assets/img/favicon.ico">
</head>
<body class="tooltips">
	
	
	<!-- Begin page -->
	<div class="container">
		<!-- Modal Dialog -->
		<!-- Start sidebar menu -->
		<!-- Start right content -->
		<?php
		include_once("static/modal.php");
		include_once("static/navigation.php");
		include_once("static/topbar.php");
		?>
		<!-- End of sidebar menu  and top bar-->



		<!-- ============================================================== -->
		<!-- Start Content here -->
		<!-- ============================================================== -->
		<div class="body content rows scroll-y">

			<!-- Page header -->
			<div class="page-heading animated fadeInDownBig">
				<?php __('reposition_page_title'); ?>
			</div>
			<!-- End page header -->


			<!-- Your awesome content goes here -->
			<div class="row">
				<div class="col-sm-6">
					<div class="box-info">
						<h2><?php __('active_navigation'); ?></h2>
						<div class="dd" id="nestable1">
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="box-info">
						<h2><?php __('disabled_navigation'); ?></h2>
						<div class="dd" id="nestable2">
							<ol class="dd-list" style="padding:10px;">
								<li class="dd-item" ></li>
							</ol>
						</div>
					</div>
				</div>
			</div>

			<a id="save_button"  class="btn btn-default saveMenu"><?php __('save');?></a>
      <a id="reset_button"  class="btn btn-warning resetMenu"><?php __('reset_menu');?></a><img  class="loader_save_update" src="assets/img/ajax-loader.gif" />








			<textarea style="display:none;" stle="vissi" id="nestable1-output"></textarea>
			<textarea style="display:none;" id="nestable2-output"></textarea>

			<!-- End of your awesome content -->
			
			
		</div>
		<!-- ============================================================== -->
		<!-- End content here -->
		<!-- ============================================================== -->	
	</div>
	<!-- End right content -->





	<!-- the overlay modal element -->
	<div class="md-overlay"></div>
	<!-- End of eoverlay modal -->



</div>
<!-- End of page -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/third/knob/jquery.knob.js"></script>
<script src="assets/third/slimscroll/jquery.slimscroll.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="assets/third/morris/morris.js"></script>
<script src="assets/third/nifty-modal/js/classie.js"></script>
<script src="assets/third/nifty-modal/js/modalEffects.js"></script>
<script src="assets/third/sortable/sortable.min.js"></script>
<script src="assets/third/select/bootstrap-select.min.js"></script>
<script src="assets/third/summernote/summernote.js"></script>
<script src="assets/third/magnific-popup/jquery.magnific-popup.min.js"></script> 
<script src="assets/third/pace/pace.min.js"></script>
<script src="assets/third/input/bootstrap.file-input.js"></script>
<script src="assets/third/datepicker/js/bootstrap-datepicker.js"></script>
<script src="assets/third/icheck/icheck.min.js"></script>
<script src="assets/third/wizard/jquery.easyWizard.js"></script>
<script src="assets/third/wizard/scripts.js"></script>
<script src="assets/js/lanceng.js"></script>
<script src="assets/third/nestable/jquery.nestable.js"></script>

<!-- Cloud connect scripts -->
<script type="text/javascript" src="assets/scripts/cocoafish-1.2.js"></script> 
<script type="text/javascript" src="assets/scripts/utils.js"></script> 
<script type="text/javascript" src="assets/scripts/cloudconnect/objects.js"></script>
<script type="text/javascript" src="config/restaurant.json"></script>

<script>

 var settings=null;
  function fetchAndUpdateSettings(){
  	$('.loader_save_update').show();
   retreiveObjects({
    objectClass:"settings",
    query:{response_json_depth:1,page:1,per_page:50},
    listener:function(data){
      setUpSettings({
        items:data,
      })
    }
  })
 };


function resetMenu(){
    $('.loader_save_update').show();
    if(settings!=null){
    //We can save
    var fields={
      available:$('#nestable1').nestable('serialize'),
      disabled:$('#nestable2').nestable('serialize'),
    }
    

    if(settings.id){
      //We have settings before
      console.log('Modify existing settings');
      settings.navigation=null;
      updateClassObject({
        objectClass:"settings",
        fields:settings,
        id:settings.id,
        photo:null,
        listener:function(e){
          $('.loader_save_update').hide();
          window.location = 'reposition.php';
        }
      });
    }
  }else{
      alert("Please wait until the settings are fetch!")
    }

}

 function saveSettings(){
 	$('.loader_save_update').show();
  if(settings!=null){
    //We can save
    var fields={
      available:$('#nestable1').nestable('serialize'),
      disabled:$('#nestable2').nestable('serialize'),
    }
    

    if(settings.id){
      //We have settings before
      console.log('Modify existing settings');
      settings.navigation=fields;
      updateClassObject({
        objectClass:"settings",
        fields:settings,
        id:settings.id,
        photo:null,
        listener:function(e){
        	$('.loader_save_update').hide();
        }
      });
    }else{
      console.log('Create new settings');
      topublish={
        navigation:fields
      }
      console.log(topublish);

      addClassObject({
        objectClass:"settings",
        fields:topublish,
        photo:null,
        listener:function(e){
          window.location = 'reposition.php';
        }
      });

    }
  }else{
      alert("Please wait until the settings are fetch!")
    }
  }

  function createListItem(id){
  	listRow='<ol class="dd-list">';
								listRow='<li class="dd-item" data-id="'+id+'">';
									listRow+='<div class="dd-handle">'+namesOfNavigation[id]+'</div>';
								listRow+='</li>';
  	return listRow;
  }

  function createTheList(nav){
  	if(nav.available){
  		var listHandler='<ol class="dd-list">';
  		for(var i=0;i<nav.available.length;i++){
  			listHandler+=createListItem(nav.available[i].id);
  		}
  		listHandler+="</ol>";
  		$('#nestable1').html(listHandler);
  	}

  	
  		var listHandler='<ol class="dd-list">';
  		if(nav.disabled){
  			for(var i=0;i<nav.disabled.length;i++){
  				if(nav.disabled[i].id){
  					listHandler+=createListItem(nav.disabled[i].id);
  				}else if (nav.disabled.length==1){
  					nav.disabled=[];
  				}
  				
  			}
  		}
  		listHandler+="</ol>";
  		if(nav.disabled.length!=0){
  				$('#nestable2').html(listHandler);
  			}
  	



  	 // activate Nestable for list 1
    $('#nestable1').nestable({
    	group: 1,
    	maxDepth:1,
    })
    .on('change', updateOutput);

    // activate Nestable for list 2
    $('#nestable2').nestable({
    	group: 1,
    	maxDepth:1,
    })
    .on('change', updateOutput);

    
    updateOutput($('#nestable2').data('output', $('#nestable2-output')));
    updateOutput($('#nestable1').data('output', $('#nestable1-output')));
  }



function setUpSettings(data){
 $('.loader_save_update').hide();
 console.log(data.items.length);
 if(data.items.length>0){
  settings=data.items[0];

  if(settings.navigation){
    navigation=settings.navigation;
  }else {
  	navigation=theMenu;
  }
  
}else{
  settings={};
  navigation=theMenu;
}

console.log(navigation);
createTheList(navigation);
};

var updateOutput = function(e)
	{
		var list   = e.length ? e : $(e.target),
		output = list.data('output');
		if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
		} else {
			output.val('JSON browser support required for this demo.');
		}
	};



jQuery(document).ready(function() { 
	fetchAndUpdateSettings();
 	$('a.saveMenu').click(saveSettings);
  $('a.resetMenu').click(resetMenu)
    
   


});
</script>   
</body>
</html>