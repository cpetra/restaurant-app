/**
 * @author Daniel Dimov - NextWebArt  - dimovdaniel@yahoo.com
 * Titanium Restaurant App
 */

/**
 * @property Cloud
 * @type Titanium.Cloud
 */
var Cloud = require('ti.cloud');
Cloud.debug = true;
Ti.App.Properties.setBool("userLoggedIn",false);

//################### VIEWS & WINDOW ################
//TODO Add logo
//Create loading window
var loadingWindow=Ti.UI.createWindow({
        title:L('appname'),
        backgroundImage:Ti.Platform.osname=="ipad"? "/images/ipad/bg.png" : "/images/bg.png",
        navBarHidden : false,
        extendEdges : [],
        orientationModes : [Titanium.UI.PORTRAIT],
        translucent : false,
    })
    
    var logoImage = Ti.UI.createImageView({
        top:"30dp",
        image:"/images/logo.png",
    })
    loadingWindow.add(logoImage);
var actIndLoading=Ti.UI.createActivityIndicator({
            bottom:"40dp",
            width:Ti.UI.SIZE,
            height:Ti.UI.SIZE,
        })
    
//##################### LOAD SETTINGS ############
//Function to load settings from ACS
loadSettings=function(){
    Cloud.Objects.query({
        classname : 'settings',
        page : 1,
        per_page : 100,
    }, function(e) {
        if (e.success) {
            Ti.API.info(e.settings);
            actIndLoading.hide();
            Ti.App.Properties.setObject("settings",e.settings[0]);
            Ti.include('appgen.js');
        } else {
            //TODO Alert that interent connection is required
        }
    });
}

//SET UP
loadingWindow.add(actIndLoading);
actIndLoading.show();
loadingWindow.open();
loadSettings();
