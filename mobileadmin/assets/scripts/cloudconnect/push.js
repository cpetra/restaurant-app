/**
 * @param {Object} p
 * @config {Object} data
 * @config {Function} listener
 */
function sendPush(p)
{
	sdk.sendRequest('push_notification/notify.json', 'POST', p.data, function(responseData) {
		p.listener(responseData.response);
	});
}